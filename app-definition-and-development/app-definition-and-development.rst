.. index::
   pair: App Definition and Development ; cloud native landscape
   ! App Definition and Development

.. _app_definition:

=======================================================================================================
App Definition and Development
=======================================================================================================

- https://landscape.cncf.io/guide#app-definition-and-development


.. toctree::
   :maxdepth: 3

   application-definition-and-image-build/application-definition-and-image-build
