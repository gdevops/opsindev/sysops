.. index::
   pair: App Definition and Development ; Application Definition and Image build

.. _application_definition:

=======================================================================================================
Application Definition & Image build
=======================================================================================================

- https://landscape.cncf.io/guide#app-definition-and-development--application-definition-image-build

.. figure:: images/image_builders.webp

What it is ?
===============

Application definition and image build is a broad category that can be broken
down into two main subgroups.

First, developer-focused tools that help build application code into containers
and/or Kubernetes.

And second, operations-focused tools that deploy apps in a standardized way.
Whether you intend to speed up or simplify your development environment, provide
a standardized way to deploy third-party apps, or wish to simplify the process
of writing a new Kubernetes extension, this category serves as a catch-all for
a number of projects and products that optimize the Kubernetes developer and
operator experience.


CNCF projects
==============

.. toctree::
   :maxdepth: 3

   packer/packer
