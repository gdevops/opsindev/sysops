
.. _packer_intro:

=======================================================================================================
Introduction
=======================================================================================================

- https://github.com/hashicorp/packer
- https://www.packer.io/

Packer is a tool for creating identical machine images for multiple platforms
from a single source configuration.


Packer is a tool for building identical machine images for multiple platforms
from a single source configuration.

Packer is lightweight, runs on every major operating system, and is highly performant,
creating machine images for multiple platforms in parallel.

Packer supports various platforms through external plugin integrations, the full
list of which can be found at https://developer.hashicorp.com/packer/integrations.

The images that Packer creates can easily be turned into Vagrant boxes.
