.. index::
   pair: Packer ; Application Definition and Image build

.. _packer:

=======================================================================================================
Packer |packer|
=======================================================================================================

- https://landscape.cncf.io/guide?item=app-definition-and-development--application-definition-image-build--packer#app-definition-and-development--application-definition-image-build
- https://github.com/hashicorp/packer
- https://alternativeto.net/software/packer/
- https://stackshare.io/packer/alternatives

.. toctree::
   :maxdepth: 3

   introduction/introduction
