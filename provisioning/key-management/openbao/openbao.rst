.. index::
   pair: OpenBAO ; Key management
   ! OpenBAO


.. _openbao:

=======================================================================================================
**OpenBAO**
=======================================================================================================

- https://github.com/openbao/openbao


.. toctree::
   :maxdepth: 3

   introduction/introduction
