
.. _openbao_intro:

=======================================================================================================
Introduction
=======================================================================================================


OpenBao exists to provide a software solution to manage, store, and distribute
sensitive data including secrets, certificates, and keys.
