.. index::
   pair: Vault ; Key management
   ! Vault


.. _vault:

=======================================================================================================
**Vault** (A tool for secrets management, encryption as a service, and privileged access management)
=======================================================================================================

- https://landscape.cncf.io/
- https://github.com/hashicorp/vault


Tutorial
===========

- https://une-tasse-de.cafe/blog/vault/
