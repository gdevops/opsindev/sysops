
.. _fabric_description:

============================================================================
Description
============================================================================

What is Fabric ?
=======================

Fabric is a high level Python (2.7, 3.4+) library designed to execute shell
commands remotely over SSH, yielding useful Python objects in return:


.. code-block:: python

    from fabric import Connection

    result = Connection('web1.example.com').run('uname -s', hide=True)

    msg = "Ran {0.command!r} on {0.connection.host}, got stdout:\n{0.stdout}"

    print(msg.format(result))

::

    Ran 'uname -s' on web1.example.com, got stdout:
    Linux

It builds on top of Invoke (subprocess command execution and command-line features)
and Paramiko (SSH protocol implementation), extending their APIs to complement
one another and provide additional functionality.
