.. index::
   pair: fabric ; run
   pair: fabric ; cd
   pair: fabric ; put
   pair: fabric ; Connection
   pair: fabric ; SerialGroup
   pair: fabric ; fab
   pair: fabric ; fabfile.py
   pair: fabric ; environment
   pair: fabric ; variables environment
   pair: fabric ; git
   pair: fabric ; jinja2
   pair: fabric ; stderr

.. _fabric_examples:

============================================================================
**fabric commands examples** |fabric|
============================================================================

- https://docs.fabfile.org/en/latest/getting-started.html

Useof Connection, cd, run
=============================

.. code-block:: python

    from fabric import Connection

    ## Define your remote server's hostname and your username
    hostname = "load-testing"
    username = "pvergain"

    ## Create a connection to the remote server
    c = Connection(host=hostname, user=username)

    ## Run a shell command on the remote server
    with c.cd("~/informatique/terraform/tf-debian-packer/environments/bioseal"):
        result = c.run('uname -a')
        result = c.run('ls -als')

Use of Connection, cd, put, run
=================================

.. code-block:: python

    from fabric import Connection
    c = Connection('web1')
    c.put('myfiles.tgz', '/opt/mydata')
    c.run('tar -C /opt/mydata -xzvf /opt/mydata/myfiles.tgz')

You could (but don’t have to) turn such blocks of code into functions,
parameterized with a Connection object from the caller, to encourage reuse:

.. code-block:: python

    def upload_and_unpack(c):
        c.put('myfiles.tgz', '/opt/mydata')
        c.run('tar -C /opt/mydata -xzvf /opt/mydata/myfiles.tgz')

As you’ll see below, such functions can be handed to other API methods to
enable more complex use cases as well.


Use of SerialGroup, run
========================================

- https://www.fabfile.org/index.html

::

    In [6]: from fabric import SerialGroup
    In [7]: result = SerialGroup("load-testing").run('hostname')
    load-testing

    In [8]:


.. code-block:: python

    def disk_free(c):
        uname = c.run('uname -s', hide=True)
        if 'Linux' in uname.stdout:
            command = "df -h / | tail -n1 | awk '{print $5}'"
            return c.run(command, hide=True).stdout.strip()
        err = "No idea how to get disk space on {}!".format(uname)
        raise Exit(err)


::

    print(disk_free(Connection('load-testing')))
    3%



**Use of the fab command** + task, cd, run
==========================================


A fabfile.py example
-------------------------

.. code-block:: python
   :linenos:

    """fabfile.py

    Calling examples
    ===================

    - fab --list
    - fab -H machine uname
    - fab -H machine update
    """
    from fabric import task


    ## Define a task to upgrade the remote server's packages
    @task
    def uname(c):
        c.run('uname -a')


    ## Define a task to update the remote server's package index
    @task
    def update(c):
        with c.cd('/tmp'):
            c.run('sudo apt-get update')

    ## Define a task to upgrade the remote server's packages
    @task
    def upgrade(c):
        with c.cd('/tmp'):
            c.run('sudo apt-get upgrade -y')

    ## Define a task to install a package on the remote server
    @task
    def install_package(c, package_name):
        with c.cd('/tmp'):
            c.run(f'sudo apt-get install -y {package_name}')



**Use of the fab command** + task, cd, run + git + c.config.run.env (environment variables) + stderr
============================================================================================================

.. code-block:: python
   :linenos:

    """fabfile.py

    Calling examples

    - fab -H load-testing build-vm
    - fab -H load-testing update-compose--infra
    - fab -H load-testing build--api
    - fab -H load-testing load-testing

    """

    import os

    from fabric import task
    from jinja2 import Environment
    from jinja2 import FileSystemLoader


    credentials_vsphere_password = os.getenv("SUT_CREDENTIALS_VSPHERE_PASSWORD")
    vsphere_vm_cpu = os.getenv("SUT_VSPHERE_VM_CPU")  # Example: 2, 4, 8, 10
    vsphere_vm_memory = os.getenv("SUT_VSPHERE_VM_MEMORY")  # Example : 4096
    pool_size = int(os.getenv("SUT_POOL_SIZE"))
    thead_count = int(os.getenv("SUT_THREAD_COUNT"))
    xmx = int(vsphere_vm_memory) // 1024
    xmx_g = f"{xmx}g"
    memory_g = f"{xmx}g"


    command_create_terraform = (
        f"""terraform apply
        -var credentials_vpshere_user=terraform
        -var credentials_vsphere_password={credentials_vsphere_password}
        -var vsphere_vm_cpu={vsphere_vm_cpu} -var vsphere_vm_memory={vsphere_vm_memory}
        -auto-approve
        """
    ).replace("\n", " ")

    command_destroy_terraform = (
        f"""terraform destroy
        -var credentials_vpshere_user=terraform
        -var credentials_vsphere_password={credentials_vsphere_password}
        -var vsphere_vm_cpu={vsphere_vm_cpu} -var vsphere_vm_memory={vsphere_vm_memory}
        -auto-approve
        """
    ).replace("\n", " ")


    @task
    def build_debian(c):
        """Step 1. Build debian 12 template once a month"""
        try:
            command_curl = 'curl --request POST --header "PRIVATE-TOKEN: glpat-4yrxr9NXi4Ghsfvd2TC1" "https://gitlab.xx/api/v4/projects/672/pipeline?ref=benchmark_pv"'
            c.run(command_curl)
        except Exception as error:
            print(f"{error=}")


    @task
    def apply_vm(c):
        """Step 2. Create the virtual machine -benchmark.xx
        with the 2 parameters

        - vsphere_vm_cpu
        - vsphere_vm_memory

        See: https://pv_biometrics.gitlab.xx/applis/tools/locust-ci-doc/installations/sut/2-creer--benchmark/2-creer--benchmark.html
        """
        try:
            print(f"{vsphere_vm_cpu=} {vsphere_vm_memory=}")
            with c.cd("~/informatique/terraform/tf-debian-packer/environments/bioseal"):
                c.run("uname -a")
                c.run("ls -als")
                c.run(command_create_terraform)
        except Exception as error:
            print(f"{error=}")


    @task
    def destroy_vm(c):
        try:
            with c.cd("~/informatique/terraform/tf-debian-packer/environments/bioseal"):
                c.run("uname -a")
                c.run("ls -als")
                c.run(command_destroy_terraform)
        except Exception as error:
            print(f"{error=}")


    def build_docker_compose_file() -> None:
        """Part of Step 3. Build a docker-compose.yml file with a jinja2 template file
        and 5 variables:

        - vsphere_vm_memory
        - xmx
        - memory
        - pool_size
        - thead_count
        """
        env = Environment(loader=FileSystemLoader("templates"))
        compose_template = env.get_template("docker-compose.yml.jinja2")
        print(f"{pool_size=} {thead_count=} {xmx_g=} {memory_g=}")
        context = {
            "vsphere_vm_memory": vsphere_vm_memory,
            "xmx": xmx_g,
            "memory": memory_g,
            "pool_size": pool_size,
            "thead_count": thead_count,
        }
        docker_compose_file = "docker-compose.yml"
        with open(docker_compose_file, mode="w", encoding="utf-8") as compose:
            compose.write(compose_template.render(context))
            print(f"... wrote {docker_compose_file}")


    @task
    def update_compose_infra(c):
        """Step 3.1 Update the Informatique/biosdeal-infra docker-compose.yml file

        Calling update-compose-_infra task
        ============================================

        ::

            fab -H load-testing update-compose--infra

        See

        - https://gitlab.xx/Informatique/-infra/-/blob/benchmark-pv/deployment/compose/docker-compose.yml?ref_type=heads
        - https://gitlab.xx/Informatique/-infra/-/commits/benchmark-pv
        """
        try:
            build_docker_compose_file()
            with c.cd("~/informatique/-infra/deployment/compose"):
                c.run("git switch benchmark-pv")
                c.put(
                    "docker-compose.yml",
                    "/home/pvergain/informatique/-infra/deployment/compose/",
                )
                # c.run("cat docker-compose.yml")
                c.run("git add .")
                # see https://gitlab.xx/Informatique/-infra/-/commits/benchmark-pv
                c.run(
                    f"git commit -m 'Update docker-compose {pool_size=} {thead_count=} {vsphere_vm_cpu=} {vsphere_vm_memory=} {xmx=}'"
                )
                c.run("git branch")
                c.run("git push")
        except Exception as error:
            print(f"Exception: {error=}")


    @task
    def build_api(c):
        """Step 3.2 install the -api on the -benchmark.xx. Apparemment pas nécessaire.

        Calling build--api
        ============================

        ::

            fab -H load-testing build--api

        See

        - https://gitlab.xx/Informatique/-infra/-/pipelines
        """
        try:
            command_curl = 'curl --request POST --header "PRIVATE-TOKEN: XXXXXXXXXXXx" "https://gitlab.xx/api/v4/projects/207/pipeline?ref=benchmark-pv"'
            c.run(command_curl)
        except Exception as error:
            print(f"{error=}")


    @task
    def load_testing(c):
        """Step 4 the loading tests
        ::

            fab -H load-testing load-testing

        See

        - https://gitlab.xx/biometrics/applis/tools/benchmark/-/boards
        """
        try:
            with c.cd("~/biometrics/applis/tools/locust-ci"):
                c.config.run.env = {
                    "SUT_SERVER": os.getenv("SUT_SERVER"),
                    "SUT_API_KEY": os.getenv("SUT_API_KEY"),
                    "LOCUST_NB_USERS": os.getenv("LOCUST_NB_USERS"),
                    "LOCUST_RUN_TIME": os.getenv("LOCUST_RUN_TIME"),
                    "LOCUST_SPAWN_RATE": os.getenv("LOCUST_SPAWN_RATE"),
                    "LOCUST_TIMESCALE": os.getenv("LOCUST_TIMESCALE"),
                    "LOCUST_GRAFANA_URL": os.getenv("LOCUST_GRAFANA_URL"),
                    "PGHOST": os.getenv("PGHOST"),
                    "PGPORT": os.getenv("PGPORT"),
                    "PGUSER": os.getenv("PGUSER"),
                    "PGDATABASE": os.getenv("PGDATABASE"),
                    "PGPASSWORD": os.getenv("PGPASSWORD"),
                }
                result = c.run("source .venv/bin/activate; type locust; ./send_locust_commands.bash", hide=True)
                print(f"{result=}")
                for line in result.stderr.split('\n'):
                    if 'Report' in line:
                        s = line.split('Report:')
                        print(f"The report is here:{s[-1].strip()}")

        except Exception as error:
            print(f"{error=}")
