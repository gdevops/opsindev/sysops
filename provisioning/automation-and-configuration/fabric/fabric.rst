.. index::
   ! fabric

.. _fabric:

======================================================================================
**fabric** (library designed to execute shell commands remotely over SSH) |fabric|
======================================================================================

- https://github.com/fabric/fabric
- https://docs.fabfile.org/en/latest/
- https://docs.fabfile.org/en/latest/getting-started.html

.. toctree::
   :maxdepth: 3

   description/description
   examples/examples
