.. index::
   ! Automation and configuration

.. _automation:

==================================
**Automation and configuration**
==================================

- https://landscape.cncf.io/guide#provisioning--automation-configuration


.. figure:: images/automation_cncf.webp

.. toctree::
   :maxdepth: 3

   ansible/ansible
   fabric/fabric
   opentofu/opentofu
   pulumi/pulumi
   pyinfra/pyinfra
   terraform/terraform
