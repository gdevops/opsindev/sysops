
.. _pyinfra_description:

============================================================================
Description
============================================================================

- https://github.com/pyinfra-dev/pyinfra

Description
==============

pyinfra is described as 'Automates infrastructure super fast at massive scale.

It can be used for ad-hoc command execution, service deployment, configuration
management and more' and is an app in the network & admin category.

There are nine alternatives to pyinfra for a variety of platforms, including
Linux, Windows, Mac, BSD and Web-based apps.

The best pyinfra alternative is Ansible, which is both free and Open Source.
Other great apps like pyinfra are Salt, Puppet, Chef and :ref:`Fabric <fabric>`.

pyinfra alternatives are mainly Server Management Tools but may also be
Workflow Automation Tools or Network Monitors.

Filter by these if you want a narrower list of alternatives or looking for a
specific functionality of pyinfra.
