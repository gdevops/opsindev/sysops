.. index::
   pair: pyinfra ; versions

.. _pyinfra_versions:

============================================================================
Versions
============================================================================

.. toctree::
   :maxdepth: 3

   2.9.2/2.9.2
