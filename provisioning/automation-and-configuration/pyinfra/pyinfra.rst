.. index::
   ! pyinfra

.. _pyinfra:

============================================================================
**pyinfra** (automates infrastructure using Python) |pyinfra|
============================================================================

- https://github.com/pyinfra-dev/pyinfra
- https://pyinfra.com/
- https://docs.pyinfra.com/en/2.x/index.html
- https://mas.to/@fizzadar (Nick Mills-Barrett )
- https://blog.stephane-robert.info/post/introduction-pyinfra/


.. toctree::
   :maxdepth: 3

   description/description
   versions/versions
