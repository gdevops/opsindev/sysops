
.. _opentofu_faq:

==================================
FAQ
==================================


What is OpenTofu ?
====================

- https://opentofu.org/supporters/

OpenTofu is a Terraform fork, created as an initiative of Gruntwork, Spacelift,
Harness, Env0, Scalr, and others, in response to HashiCorp’s switch from an
open-source license to the BUSL.

The initiative has many supporters, all of whom are listed `here <https://opentofu.org/supporters/>`_



Why was OpenTofu created ?
==============================

The BUSL and the additional use grant outlined by the HashiCorp team are ambiguous,
which makes it challenging for companies, vendors, and developers using Terraform
to decide whether their actions could be interpreted as being outside the permitted
scope of use.

Hashicorp’s FAQs give some peace of mind to end users and system integrators
for now, but the licensing terms’ implications for future usage are unclear.

The possibility that the company’s definition of “competitive” or “embedding”
could change or the license could be further modified to make it closed source
prompts uncertainty for Terraform users.

We firmly believe that Terraform should remain open-source because it is a project
many companies use, and many contributors have made Terraform what it is today.

Terraform’s success would not have been possible without the community’s work to
build many supporting projects around it.

What are the differences between OpenTofu and Terraform ?
================================================================

On the technical level, OpenTofu 1.6.x is very similar feature-wise to
Terraform 1.6.x. In the future, the projects feature sets will diverge.

The other main difference is that OpenTofu is open-source, and it's goal is to
be driven in a collaborative way with no single company being able to dictate
the roadmap.

Why should you use OpenTofu instead of Terraform ?
======================================================

Personal use
-----------------

Initial impressions suggest you could use either OpenTofu or Terraform for
personal use, as the BUSL license has no restrictions for non-commercial use cases.

That may change as the Terraform ecosystem becomes increasingly unstable, and a
switch to another license may happen.

Those familiar with Terraform will have no issues adopting OpenTofu for personal
use, so there will be no knowledge gaps, at least at the start.

Consultants
---------------

A consultant should offer their clients the best possible solution that aligns
with their budget.

OpenTofu will be on par with Terraform, and one of the project’s central objectives
is to listen to the community’s issues, so it makes sense to recommend a project
that will always stay open-source.
Anyone who has used Terraform in the last eight years has probably come across
issues that took some time to be resolved.

The large community involved in developing OpenTofu means this will no longer
be the case.

Companies
------------------

Companies will encounter more difficulties with the situation.

Switching to a new project carries risks, but staying with a project that changes
its license without warning is far riskier.

This risk is minimized by giving OpenTofu to the Linux Foundation, and OpenTofu’s
aim of maintaining feature parity with Terraform for future releases reduces
the technical risks.

Will OpenTofu be compatible with future Terraform releases ?
=================================================================

The community will decide what features OpenTofu will have.

Some long-awaited Terraform features will be publicly available soon.

If you're missing a feature in OpenTofu that's available in Terraform, feel free
to create an issue.


Can I use OpenTofu as a drop-in replacement for Terraform? Is OpenTofu suitable for production use ?
=========================================================================================================

Right now, OpenTofu is a drop-in replacement for Terraform, as it's compatible
with Terraform versions 1.5.x and most of 1.6.x.

You don’t need to make any changes to your code to ensure compatibility.

OpenTofu is suitable for production use cases without any exception.

Please see our `migration guide  <https://opentofu.org/docs/intro/migration/>`_ for more information.

