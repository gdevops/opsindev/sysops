.. index::
   ! Opentofu

.. _opentofu:

===================================================================================
**Opentofu** lets you declaratively manage your cloud infrastructure |opentofu|
===================================================================================

- https://github.com/opentofu/opentofu
- https://github.com/orgs/opentofu/discussions
- https://opentofu.org/docs/
- https://opentofu.org/manifesto/
- https://www.youtube.com/@OpenTofu/videos
- https://github.com/opentofu/opentofu/blob/main/WEEKLY_UPDATES.md
- https://landscape.cncf.io/guide?item=provisioning--automation-configuration--opentofu#provisioning--automation-configuration
- https://github.com/cncf/landscape/commit/889700c406a87d6a44b657a367eac8dfb0c658bc

.. toctree::
   :maxdepth: 3

   introduction/introduction
   faq/faq
   versions/versions
