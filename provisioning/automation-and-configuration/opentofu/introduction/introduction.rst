
.. _opentofu_intro:

==================================
Introduction
==================================


What is OpenTofu ?
====================

- https://opentofu.org/supporters/

OpenTofu is a Terraform fork, created as an initiative of Gruntwork, Spacelift,
Harness, Env0, Scalr, and others, in response to HashiCorp’s switch from an
open-source license to the BUSL.

The initiative has many supporters, all of whom are listed `here <https://opentofu.org/supporters/>`_

Why should you use OpenTofu instead of Terraform ?

Definition
==============

OpenTofu is an infrastructure as code tool that lets you define both cloud and
on-prem resources in human-readable configuration files that you can version,
reuse, and share.
It is hosted by the Linux Foundation (LF).


Additional repos in scope of the application

- https://github.com/opentofu/manifesto
- https://github.com/opentofu/opentofu.org
- https://github.com/opentofu/brand-artifacts
- https://github.com/opentofu/registry
- https://github.com/opentofu/scripts

Website URL
===============

- https://opentofu.org/

Roadmap
=======

- https://github.com/opentofu/opentofu/milestones

Roadmap context
===================

The project’s team decided to keep the roadmap as GitHub milestones, which gives
full transparency to the process and the current state of the process.

Additionally, team provides weekly updates to public knowledge: https://github.com/opentofu/opentofu/blob/main/WEEKLY_UPDATES.md

Roadmap is agreed and confirmed by Steering Committee, and information about
decisions is also publicly available: https://github.com/opentofu/opentofu/blob/main/TSC_SUMMARY.md
