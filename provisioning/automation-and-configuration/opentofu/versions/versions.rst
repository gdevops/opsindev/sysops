.. index::
   pair: Opentofu ; Versions

.. _opentofu_versions:

=========================================================================
Versions
=========================================================================

- https://github.com/opentofu/opentofu


.. toctree::
   :maxdepth: 3

   1.6.2/1.6.2
   1.6.0/1.6.0
