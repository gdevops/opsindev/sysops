.. index::
   ! Pulumi

.. _pulimi:

=========================================================================================================================================================
**pulimi** Infrastructure as Code in any programming language. Build infrastructure intuitively on any cloud using familiar languages
=========================================================================================================================================================

- https://github.com/pulumi/pulumi
- https://www.pulumi.com/
- https://landscape.cncf.io/guide?item=provisioning--automation-configuration--pulumi#provisioning--automation-configuration
- https://blog.stephane-robert.info/docs/infra-as-code/provisionnement/pulumi/introduction/

.. toctree::
   :maxdepth: 3
