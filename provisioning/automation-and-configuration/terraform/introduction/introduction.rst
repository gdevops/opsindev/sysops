.. index::
   pair: Terraform ; Introduction

.. _terraform_intro:

==================================
Introduction
==================================


Par Stéphane Robert
========================

- https://blog.stephane-robert.info/docs/infra-as-code/provisionnement/terraform/introduction/


Terraform, développé par HashiCorp et lancé en 2014, a révolutionné le concept
d'Infrastructure as Code (IAC).

Son émergence a été motivée par la complexité croissante des architectures
informatiques et la nécessité d'une gestion plus agile et efficace des ressources
dans le cloud.

Avant Terraform, la gestion de l'infrastructure était souvent fragmentée et dépendante
de multiples outils spécifiques à chaque fournisseur de cloud ou à chaque technologie.

Cette approche cloisonnée générait des défis significatifs en termes de portabilité
et d'efficacité opérationnelle.

Les administrateurs systèmes faisaient face à des processus manuels sujets aux erreurs,
entraînant des déploiements lents et des inconsistances.

Mitchell Hashimoto et Armon Dadgar, les fondateurs de HashiCorp, ont perçu ces défis
comme une opportunité pour créer un outil unifié qui simplifierait la gestion de
l'infrastructure.

Terraform a été conçu pour permettre aux utilisateurs de définir et de provisionner
l'infrastructure à l'aide d'un langage de configuration simple et déclaratif.

Cette approche visait à rendre les configurations reproductibles et à faciliter
la gestion de versions, un élément indispensable.
