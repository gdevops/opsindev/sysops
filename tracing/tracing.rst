
.. index::
   pair: Tracing ; Management
   ! Tracing

.. _tracing:

==========================
Tracing
==========================

.. seealso::

   - https://en.wikipedia.org/wiki/Comparison_of_network_monitoring_systems
   - https://en.wikipedia.org/wiki/Computer_and_network_surveillance
   - https://openapm.io/landscape
   - https://x.com/openapmio
   - https://landscape.cncf.io/
   - https://landscape.cncf.io/category=tracing&format=card-mode&grouping=category

.. figure:: cncf_tracing.png
   :align: center

   https://landscape.cncf.io/category=tracing&format=card-mode&grouping=category


.. toctree::
   :maxdepth: 6

   jaeger/jaeger
