
.. index::
   pair: Versions ; Jaeger

.. _jaeger_versions:

=======================================================
Jaeger versions
=======================================================

.. seealso::

   - https://github.com/jaegertracing/jaeger/releases


.. toctree::
   :maxdepth: 3

   1.17.1/1.17.1
