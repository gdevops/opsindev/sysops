
.. _jaeger_def:

=======================================================
Jaeger definition
=======================================================

.. seealso::

   - https://www.jaegertracing.io/

.. contents::
   :depth: 3

Why Jaeger ?
==============

As on-the-ground microservice practitioners are quickly realizing, the
majority of operational problems that arise when moving to a distributed
architecture are ultimately grounded in two areas: networking and observability.

It is simply an orders of magnitude larger problem to network and debug
a set of intertwined distributed services versus a single monolithic
application.
