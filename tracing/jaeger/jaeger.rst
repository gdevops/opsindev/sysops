
.. index::
   pair: Tracing ; Jaeger
   ! Jaeger

.. _jaeger:

=======================================================
Jaeger (CNCF Jaeger, a Distributed Tracing Platform)
=======================================================

.. seealso::

   - https://github.com/jaegertracing/jaeger
   - https://www.jaegertracing.io/
   - https://x.com/JaegerTracing
   - https://x.com/YuriShkuro (Creator)
   - https://www.shkuro.com/books/2019-mastering-distributed-tracing/
   - https://landscape.cncf.io/category=tracing&format=card-mode&grouping=category


.. figure:: jaeger_logo.jpeg
   :align: center

.. figure:: cncf_jaeger.png
   :align: center

   https://landscape.cncf.io/category=tracing&format=card-mode&grouping=category&selected=jaeger


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
