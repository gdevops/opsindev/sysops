
.. index::
   ! Glossaire

.. _glossaire:

==========================
Glossaire
==========================

- https://glossary.cncf.io/fr/
- https://github.com/cncf/glossary
- https://blog.stephane-robert.info/docs/glossaire/introduction/

.. glossary::

   bioseal
   BioSeal

       - https://bioseal.codes/

       BioSeal is a global and interoperable solution to certify and authenticate
       all types of products and documents, making them tamper-proof.

       Thanks to instant and local verification, it helps to fight against fraudulent
       falsification of all types of documents and to verify the identity of
       people thanks to privacy-friendly biometric recognition technology

       BioSeal is labelled France Cybersecurity

       BioSeal est un code-barres 2D permettant à la fois de sécuriser les données
       clés d’un document physique ou numérique et d’identifier par la biométrie
       le propriétaire du document.


   CNCF
       Cloud Native Computing Foundation

   FQDN
       A fully qualified domain name (FQDN) is the complete domain name
       for a specific computer, or host, on the internet.

       The FQDN consists of two parts: the hostname and the domain name.

       For example, an FQDN for a hypothetical mail server might be
       mymail.somecollege.edu.

       The hostname is mymail, and the host is located within the domain
       somecollege.edu.

       Source: https://kb.iu.edu/d/aiuv

   HCL
       HashiCorp Configuration Language

       - https://blog.stephane-robert.info/docs/developper/autres-langages/hcl/introduction/

       Dans le vaste univers des outils DevOps dédiés à l'automatisation et à la
       gestion de l'infrastructure, il est courant de croiser des langages tels
       que JSON ou YAML.

       Cependant, la société HashCorp a fait le choix de proposer son propre langage :
       le HCL pour HashiCorp Configuration Language. On le retrouve ainsi sur les
       outils tels que Terraform, Packer, Consul et Vault.

       La syntaxe HCL est inspirée de libucl, de la configuration nginx et d'autres.

   HSM
       Hardware Security Module

       - https://fr.wikipedia.org/wiki/Hardware_Security_Module

       Un Hardware Security Module ou HSM (en français, boîte noire transactionnelle ou BNT)
       est un matériel électronique offrant un service de sécurité qui consiste
       à générer, stocker et protéger des clefs cryptographiques.

       Ce matériel peut être une carte électronique enfichable PCI sur un ordinateur
       ou un boîtier externe SCSI/IP par exemple.


   IAC
       Infrastructure As Code

       - https://blog.stephane-robert.info/docs/infra-as-code/introduction/

       Avec l'essor de la démarche DevOps, la nécessité de pouvoir provisionner
       et gérer les infrastructures informatiques rapidement a conduit à la création
       d'un nouveau type d'outils.

       L'infrastructure en tant que code (IaC) incarne cette évolution, offrant
       une approche automatisée pour le provisionnement et la gestion des ressources
       informatiques.

       Ce type d'outils transforme la gestion des infrastructures en permettant
       aux équipes de les traiter avec du code informatique, semblable à celui
       utilisé pour le développement de logiciels.
       Cette innovation favorise une relation plus étroite et plus synergique
       entre le développement (Dev) et les opérations (Ops) dans le cadre de la
       culture DevOps.

       Au cœur de cette révolution se trouvent des outils comme Pulumi, Terraform
       et Ansible.
       Chacun de ces outils apporte une perspective unique à l'IaC.

       `Pulumi <https://blog.stephane-robert.info/docs/infra-as-code/provisionnement/pulumi/introduction/>`_ , par exemple, permet aux développeurs d'utiliser des langages de
       programmation familiers pour définir et déployer l'infrastructure, tandis
       que Terraform utilise un langage déclaratif pour décrire l'état souhaité
       de l'infrastructure.

       `Ansible <https://blog.stephane-robert.info/docs/infra-as-code/gestion-de-configuration/ansible/introduction/>`_, quant à lui, se concentre sur la gestion de configuration automatisée,
       facilitant la maintenance et le déploiement de configurations sur diverses machines.


   Machines Virtuelles
   machines virtuelles
   machine virtuelle
   Machine Virtuelle
       - https://glossary.cncf.io/fr/virtual-machine/

       Une machine virtuelle (VM) est un ordinateur et son système d’exploitation
       qui n’est pas lié à un matériel particulier.

       Les VMs s’appuient sur la virtualisation pour découper un unique ordinateur
       physique en plusieurs ordinateurs virtuels.

       Cette séparation permet aux entités et aux fournisseurs d’infrastructure
       de facilement créer et détruire des VMs sans impacter l’infrastructure sous-jacente

       **Problème auquel répond la machine virtuelle**

       Les machines virtuelles tirent parti de la virtualisation.
       Lorsqu’une machine physique est liée à un unique système d’exploitation,
       la manière dont les ressources peuvent être utilisées est en quelque sorte
       limitée.
       De plus, lorsqu’un système d’exploitation est lié à une unique machine physique,
       sa disponibilité est directement liée à ce matériel.
       Si la machine physique est indisponible à cause d’une maintenance ou d’un
       problème matériel, le système d’exploitation l’est également.

       **Quelle est l’utilité d'une machine virtuelle**

       En supprimant la relation directe entre un système d’exploitation et la
       machine physique unique, vous résolvez plusieurs problèmes des machines
       physiques: temps de provisionning, utilisation du matériel et résiliance.

       Sans avoir besoin d’acheter, d’installer ou du configurer du nouveau matériel,
       le temps de provisionning d’un nouvel ordinateur est considérablement amélioré.

       Les VMs vous permettent de mieux utiliser vos ressources matérielles
       existantes en plaçant plusieurs machines virtuelles sur une seule machine
       physique.

       N’étant pas lié à une machine physique particulière, les VMs sont également
       plus résiliantes que les machines physiques.

       Lorsqu’une machine physique a besoin d’être indisponible, les VMs qui sont
       éxécutées dessus sont déplacées sur une autre machine avec peu ou pas
       d’indisponibilité.


   reverse proxy
   proxy inverse
       Un proxy inverse (reverse proxy) est un type de serveur, habituellement
       placé en frontal de serveurs web.

       Contrairement au serveur proxy qui permet à un utilisateur d'accéder
       au réseau Internet, le proxy inverse permet à un utilisateur d'Internet
       d'accéder à des serveurs internes, une des applications courantes
       du proxy inverse est la répartition de charge (load-balancing).

       Le proxy inverse est installé du côté des serveurs Internet.
       L'utilisateur du Web passe par son intermédiaire pour accéder aux
       applications de serveurs internes.
       Le **proxy inverse** est parfois appelé **substitut (surrogate)**

       .. seealso:: https://fr.wikipedia.org/wiki/Proxy_inverse

   Virtualisation
       - https://glossary.cncf.io/fr/virtualization

       La virtualisation, dans un cadre Cloud Native, est l’exécution de plusieurs
       systèmes d’exploitation isolés sur un même ordinateur physique, aussi
       appelé serveur.

       Ces systèmes d’exploitation isolés et leurs ressources de calcul dédiées
       (processeur, mémoire et réseau) sont appelées machines virtuelles ou VMs.

       Lorsque nous parlons d’une machine virtuelle, nous parlons d’un ordinateur
       défini par logiciel.
       C’est-à-dire quelque chose qui ressemble et se comporte comme un vrai ordinateur,
       mais partage son matériel avec d’autres machines virtuelles.

       Le cloud computing fonctionne principalement grâce à la virtualisation.

       Par exemple, lorsque vous louez un “ordinateur” auprès d’AWS, cet ordinateur
       est en fait une machine virtuelle.

       **Problème auquel répond la virtualisation**

       La virtualisation résout plusieurs problèmes, notamment l’amélioration de
       l’utilisation du matériel physique en permettant à plus d’applications
       de s’exécuter sur un même ordinateur physique tout en restant isolés les
       uns des autres pour des raisons de sécurité.

       **Quelle est l’utilité de la virtualisation**

       Les applications exécutées sur des machines virtuelles ne savent pas
       qu’elles partagent un ordinateur physique.

       La virtualisation permet également de démarrer un nouvel “ordinateur”
       (une machine virtuelle) en quelques minutes sans se soucier des contraintes
       physiques liées à l’ajout d’un nouvel ordinateur dans un centre de données.

