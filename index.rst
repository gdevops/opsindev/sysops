
.. figure:: _static/sysops.png
   :align: center

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/opsindev/sysops/rss.xml>`_

.. _tuto_sysops:

========================================
|fabric| **devops/sysops** |cncf|
========================================

- https://fr.wikipedia.org/wiki/Administrateur_syst%C3%A8me
- https://en.wikipedia.org/wiki/System_administrator
- https://github.com/actionjack/so-you-want-to-onboard-a-devops-engineer
- https://landscape.cncf.io/ |cncf|


.. toctree::
   :maxdepth: 7

   awsome/awsome
   app-definition-and-development/app-definition-and-development
   dns/dns
   provisioning/provisioning
   inventory/inventory
   ldap/ldap
   observability-and-analysis/observability-and-analysis
   raid/raid
   service_mesh/service_mesh
   service_proxy/service_proxy
   tracing/tracing
   glossaire/glossaire
