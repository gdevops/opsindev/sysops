

.. index::
   pair: Stéphane Bortzmeyer; LDAP


.. _ldap_def:

===============================================
LDAP definition
===============================================

.. seealso::

   - http://www.linux-france.org/prj/edu/archinet/systeme/ch51s02.html

.. contents::
   :depth: 3

English wikipedia definition
=============================

.. seealso::

   - https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol


The Lightweight Directory Access Protocol (LDAP /ˈɛldæp/) is an open,
vendor-neutral, industry standard application protocol for accessing and
maintaining distributed directory information services over an Internet
Protocol (IP) network.

Directory services play an important role in developing intranet and
Internet applications by allowing the sharing of information about
users, systems, networks, services, and applications throughout the
network.

As examples, directory services may provide any organized set of records,
often with a hierarchical structure, such as a corporate email directory.

Similarly, a telephone directory is a list of subscribers with an address
and a phone number.

LDAP is specified in a series of Internet Engineering Task Force (IETF)
Standard Track publications called Request for Comments (RFCs), using
the description language ASN.1.

The latest specification is Version 3, published as RFC 4511 (a road map
to the technical specifications is provided by RFC4510).

**A common use of LDAP is to provide a central place to store usernames
and passwords**.

This allows many different applications and services to connect to the
LDAP server to validate users.

LDAP is based on a simpler subset of the standards contained within
the X.500 standard. Because of this relationship, LDAP is sometimes
called X.500-lite


French wikipedia definition
=============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol

Lightweight Directory Access Protocol (LDAP) est à l'origine un protocole
permettant l'interrogation et la modification des services d'annuaire
(il est une évolution du protocole DAP).

Ce protocole repose sur TCP/IP.

Il a cependant évolué pour représenter une norme pour les systèmes
d'annuaires, incluant un modèle de données, un modèle de nommage,
un modèle fonctionnel basé sur le protocole LDAP, un modèle de sécurité
et un modèle de réplication.

C'est une structure arborescente dont chacun des nœuds est constitué
d'attributs associés à leurs valeurs.

LDAP est moins complexe que le modèle X.500 édicté par l'UIT-T.

Le nommage des éléments constituant l'arbre (racine, branches, feuilles)
reflète souvent le modèle politique, géographique ou d'organisation de
la structure représentée.

La tendance actuelle est d'utiliser le nommage DNS pour les éléments
de base de l'annuaire (racine et premières branches, domain components
ou dc=…).

Les branches plus profondes de l'annuaire peuvent représenter des unités
d'organisation ou des groupes (organizational units ou ou=…),
des personnes (common name ou cn=… voire user identifier uid=…).

L'assemblage de tous les composants (du plus précis au plus général)
d'un nom forme son distinguished name,

l'exemple suivant en présente deux :

- cn=ordinateur,ou=machines,dc=EXEMPLE,dc=FR
- cn=Jean,ou=personnes,dc=EXEMPLE,dc=FR


.. _definition_bortzmeyer:

Définition par Stéphane Bortzmeyer
===================================


.. seealso::

   - http://www.bortzmeyer.org/comptes-unix-ldap.html

Si on gère de nombreux ordinateurs multi-utilisateurs, on se pose forcément la
question de la centralisation des comptes, des informations sur les utilisateurs.

Il est très pénible de devoir créer un compte sur N machines dès qu'un nouvel
employé arrive ou de devoir se rappeler des M mots de passe précédents parce
qu'on ne les a pas changé sur toutes les machines. Il existe plusieurs solutions
techniques à ce problème, et je présente ici l'utilisation de LDAP pour gérer
une base centralisée de comptes, notamment pour des machines Unix.

LDAP est un protocole client-serveur d'interrogation d'un annuaire. Il offre
donc quelques analogies avec le DNS. Mais son histoire est très différente.
Issu à l'origine du projet X.500, il en est aujourd'hui le seul survivant.
LDAP est normalisé dans les RFC 4510 et suivants.

LDAP est juste un protocole, il n'impose rien sur le mécanisme de stockage
des données, dont on dira un mot au moment de configurer le serveur.
Mais nous allons commencer par la configuration du client, car il y a davantage
de clients que de serveurs.

Nous avons donc une machine Unix (en l'occurrence une Debian) qui a une base
d'utilisateurs traditionnelle (le fichier /etc/passwd) et qui voudrait
l'enrichir avec les informations venues du serveur LDAP ldap.example.org.
L'administrateur de ce serveur nous a donné le nom du serveur et la base à
utiliser, dc=example,dc=org (contrairement au DNS, LDAP ne part pas de la
racine mais d'une base, souvent dérivée d'un nom de domaine).
L'administrateur nous dira également la version utilisée (la version 3 reste
aujourd'hui la seule) et le mécanisme d'authentification auprès du serveur
(contrairement au DNS, un serveur LDAP peut nécessiter une authentification
des clients), l'usage ou non de TLS, etc.
Les objets LDAP décrivant les utilisateurs seront probablement de la
classe posixAccount.

Que faut-il connaitre sur LDAP encore ? Outre les références à la fin de cet
article, nécessaires pour tout savoir, notons simplement que chaque objet
stocké dans la base LDAP a un :term:`DN` (Distinguished Name) et que ce DN l'identifie
de manière unique.
Le DN est formé de plusieurs composants, chaque composant étant un doublet
attribut=valeur.

Par exemple sur example.org::

    uid=bleriot,ou=People,dc=example,dc=org

est un :term:`DN` (Distinguished Name).


Autres définitions du protocole LDAP
======================================

Ensimag
---------

.. seealso::

   - https://ensiwiki.ensimag.fr/index.php/About_LDAP

LDAP uses a client/server approach, by connected mode with the TCP protocol,
on the port 389 (default port).
The transcription uses the BER (Basic Encoding Rule) codage for fast transfers
on the network.
It can accept TLS (Transport Layer Security) for the transfers confidentiallity
(with the port 636).
The standard allows many operations (control and extents operation) defined
by RFC (Request For Comment).


LDAP vu par OpenLDAP,  GNU/Linux Magazine/France N°195, juillet/août 2016
----------------------------------------------------------------------------


.. note:: leur définition n'est pas très correcte puisque LDAP est un
   protocole mais leur définition est plus abordable.

Un **annuaire LDAP** est un annuaire électronique, composé d'un ou de plusieurs
arbres de données qui centralisent les informations de l'entreprise.
Cette structure hiérarchique est appelée DIT (Directory Information Tree).
Chaque entrée de l'arbre est un ensemble d'attributs et dispose d'un
identifiant unique: son :term:`DN` (Distinguished Name).

Dans les versions récentes d'openLDAP (supérieures à la version 2.4), la
configuration n'est plus stockée dans un fichier de configuration plat, mais
réside directement dans la base de données elle-même, au sein d'une DIT
spécifique.
C'est la configuration *OLC* (On-Line Configuration), également connue
sous le nom de **cn=config**.

Evitez tout tutoriel commençant par quelque chose du genre::

    modifier le fichier slapd.conf ...


Préférez l'usage des commandes *ldapmodify* ou *ldapadd*.
