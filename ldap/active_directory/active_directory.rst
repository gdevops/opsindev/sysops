
.. index::
   ! Active Directory
   ! AD

.. _active_directory:

===============================================
Active Directory
===============================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Active_Directory
   - https://docs.microsoft.com/fr-fr/windows-server/identity/ad-ds/active-directory-domain-services

.. toctree::
   :maxdepth: 7

   definition/definition
