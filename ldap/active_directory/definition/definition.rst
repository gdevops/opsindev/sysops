
.. index::
   pair: Active Directory ; Definition
   pair: Active Directory ; Contrôleur de domaine
   pair: domain controller ; Contrôleur de domaine

.. _active_directory_def:

===============================================
Active Directory definition
===============================================

.. contents::
   :depth: 3


English wikipedia definition
=============================

.. seealso::

   - https://en.wikipedia.org/wiki/Active_Directory

Active Directory (AD) is a directory service that Microsoft developed
for the Windows domain networks.

It is included in most Windows Server operating systems as a set of
processes and services.

Initially, Active Directory was only in charge of centralized domain
management.

Starting with Windows Server 2008, however, Active Directory became an
umbrella title for a broad range of **directory-based identity-related**
services.

A server running Active Directory Domain Service (AD DS) is called a
**domain controller**.
It authenticates and authorizes all users and computers in a Windows
domain type network—assigning and enforcing security policies for all
computers and installing or updating software.

For example, when a user logs into a computer that is part of a Windows
domain, Active Directory checks the submitted password and determines
whether the user is a system administrator or normal user.

Also, it allows management and storage of information, provides
**authentication and authorization** mechanisms, and establishes a framework
to deploy other related services: Certificate Services,
Active Directory Federation Services, Lightweight Directory Services
and Rights Management Services.

Active Directory uses Lightweight Directory Access Protocol (LDAP)
versions 2 and 3, Microsoft's version of Kerberos, and DNS.


French Wikipedia definition
==============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Active_Directory

Active Directory (AD) est la mise en œuvre par Microsoft des services
d'annuaire LDAP pour les systèmes d'exploitation Windows.

L'objectif principal d'Active Directory est de fournir des services
centralisés d'identification et d'authentification à un réseau
d'ordinateurs utilisant le système Windows.

Il permet également l'attribution et l'application de stratégies,
l'installation de mises à jour critiques par les administrateurs.

Active Directory répertorie les éléments d'un réseau administré tels
que les comptes des utilisateurs, les serveurs, les postes de travail,
les dossiers partagés (en), les imprimantes, etc.

Un utilisateur peut ainsi facilement trouver des ressources partagées,
et les administrateurs peuvent contrôler leur utilisation grâce à des
fonctionnalités de distribution, de duplication, de partitionnement
et de sécurisation de l’accès aux ressources répertoriées.

Si les administrateurs ont indiqué les attributs convenables, il sera
possible d'interroger l'annuaire pour obtenir, par exemple,
« toutes les imprimantes couleurs à cet étage du bâtiment ».

Le service d'annuaire Active Directory peut être mis en œuvre sur
Windows 2000 Server, Windows Server 2003, Windows Server 2008,
Windows Server 2012 (voire hors Microsoft par Samba)
et Windows Server 2016, il résulte de l'évolution de la base de
compte plane SAM.

Un serveur informatique hébergeant l'annuaire Active Directory est
appelé « contrôleur de domaine ».

Active Directory stocke ses informations et paramètres dans une base de
données distribuée sur un ou plusieurs contrôleurs de domaine,
la réplication étant prise en charge nativement.

La taille d'une base Active Directory peut varier de quelques centaines
d'objets, pour de petites installations, à plusieurs millions d'objets,
pour des configurations volumineuses.
