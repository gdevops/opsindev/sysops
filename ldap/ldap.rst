
.. index::
   ! Lightweight Directory Access Protocol
   ! LDAP

.. _ldap:

===============================================
LDAP
===============================================

LDAP: Lightweight Directory Access Protocol

.. seealso::

   - https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol

.. toctree::
   :maxdepth: 7

   definition/definition
   software/software
   active_directory/active_directory
   glossary/glossary
   versions/versions
