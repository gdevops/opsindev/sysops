
.. index::
   pair: Openldap ; LDAP

.. _openldap:

===============================================
Openldap
===============================================

.. seealso::

   - https://en.wikipedia.org/wiki/OpenLDAP
   - https://www.openldap.org/devel/gitweb.cgi?p=openldap.git;a=tree
   - https://github.com/openldap/openldap
   - https://www.openldap.org/


.. figure:: OpenLDAP-logo.png
   :align: center


.. toctree::
   :maxdepth: 3


   definition/definition
   commands/commands
   versions/versions
