
.. index::
   pair: Openldap ; Versions

.. _openldap_versions:

===============================================
Openldap versions
===============================================

.. seealso::

   - https://www.openldap.org/software/release/changes.html
   - https://github.com/openldap/openldap/releases


.. toctree::
   :maxdepth: 3

   2.4.47/2.4.47
