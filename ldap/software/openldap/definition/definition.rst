

.. _openldap_def:

===============================================
Openldap definition
===============================================

.. contents::
   :depth: 3

English wikipedia definition
==============================

.. seealso::

   - https://en.wikipedia.org/wiki/OpenLDAP

OpenLDAP is a free, open-source implementation of the Lightweight Directory
Access Protocol (LDAP) developed by the OpenLDAP Project.

It is released under its own BSD-style license called the OpenLDAP
Public License.

LDAP is a platform-independent protocol.

Several common Linux distributions include OpenLDAP Software for LDAP
support.

The software also runs on BSD-variants, as well as AIX, Android, HP-UX,
macOS, Solaris, Microsoft Windows (NT and derivatives, e.g. 2000, XP,
Vista, Windows 7, etc.), and z/OS.

French wikipedia definition
==============================

.. seealso::

   - https://fr.wikipedia.org/wiki/OpenLDAP

OpenLDAP est une implémentation libre du protocole LDAP maintenue par le
projet OpenLDAP et distribuée selon les termes de la licence OpenLDAP
Public Licence.

Outre le code source, on trouve des versions compilées pour GNU/Linux,
FreeBSD, NetBSD, OpenBSD, AIX, HP-UX, Mac OS X, Solaris, et Microsoft
Windows (2000, XP).
