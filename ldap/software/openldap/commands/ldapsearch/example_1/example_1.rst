
.. index::
   pair: LDAP ; users
   pair: Attribute ; sAMAccountName
   pair: Attribute ; sn
   pair: Attribute ; mail


.. _ldapsearch_example_1:

===============================================
ldapsearch example 1
===============================================

.. contents::
   :depth: 3


.. _ldapsearch_users:

users
========

.. seealso::

   - https://access.redhat.com/documentation/en-US/Red_Hat_Directory_Server/8.2/html/Administration_Guide/Examples-of-common-ldapsearches.html


.. contents::
   :depth: 3


TL;DR
=======

Searching the sAMAccountName="mkent" and display all attributes
----------------------------------------------------------------------------

- :ref:`ldapsearch_sama_account_mkent`

::

    ldapsearch -x -v -h pdc.int.fsf.de \
    -D "<username>" -w "<password>"  \
    -b ou=users,ou="company",dc=int,dc=fsf,dc=de  \
    -s sub "(sAMAccountName=mkent)"



Searching the users with "cn" attribute selection
-------------------------------------------------------

- :ref:`fsf_users_cn`

::

        ldapsearch -x -v -h pdc.int.fsf.de \
         -D "<username>" -w "<password>" \
         -b ou=users,ou="company",dc=int,dc=fsf,dc=de \
         -s sub cn | grep "cn: "


Searching CN="Martha Kent" and display all attributes
----------------------------------------------------------------------

- :ref:`fsf_attributes_pv`

::

    ldapsearch -x -v -h pdc.int.fsf.de \
    -D "<username>" -w "<password>"  \
    -b ou=users,ou="company",dc=int,dc=fsf,dc=de  \
    CN="Martha Kent"


or, giving the CN with the complete path::

    ldapsearch -x -v -h pdc.int.fsf.de \
    -D "<username>" -w "<password>"  \
    -b cn="Martha Kent",OU=New York,ou=users,ou="company",dc=int,dc=fsf,dc=de




Searching sAMAccountName=mkent and display sAMAccountName, sn et mail attributes
----------------------------------------------------------------------------------------------

- :ref:`fsf_ldapsearch_sama_account_sn_mail_pv`


::

        ldapsearch -x -v -h pdc.int.fsf.de \
        -D "<username>" -w "<password>"  \
        -b ou=users,ou="company",dc=int,dc=fsf,dc=de  \
        -s sub "(sAMAccountName=mkent)" \
        -s sub sAMAccountName sn mail


Searching CN="Martha Kent" and display AMAccountName, sn et mail attributes
-----------------------------------------------------------------------------

- :ref:`fsf_attributes_account_sn_mail_pv`

::

    ldapsearch -x -v -h pdc.int.fsf.de \
    -D "<username>" -w "<password>"  \
    -b ou=users,ou="company",dc=int,dc=fsf,dc=de  \
    CN="Martha Kent" -s sub sAMAccountName sn mail

or, giving the CN in the search base with the complete path::


    ldapsearch -x -v -h pdc.int.fsf.de \
    -D "<username>" -w "<password>"  \
    -b cn="Martha Kent",OU=New York,ou=users,ou="company",dc=int,dc=fsf,dc=de \
    -s sub sAMAccountName sn mail


Searching the users with the "sAMAccountName" selection
--------------------------------------------------------------------


::

        ldapsearch -x -v -h pdc.int.fsf.de \
        -D "<username>" -w "<password>"  \
        -b ou=users,ou="company",dc=int,dc=fsf,dc=de \
        -s sub sAMAccountName | grep "sAMAccountName: "



List of cn="Martha Kent" groups
------------------------------------------------------------

- :ref:`fsf_groups_pv`

::


        ldapsearch -x -v -h pdc.int.fsf.de \
        -D "<username>" -w "<password>"  \
        -b ou=users,ou="company",dc=int,dc=fsf,dc=de  \
        CN="Martha Kent" -s sub memberOf


List of group **grp_heroes** members
-----------------------------------------------

::

        ldapsearch -x -v -h pdc.int.fsf.de \
        -D "<username>" -w "<password>"  \
        -b ou=groups,ou="company",dc=int,dc=fsf,dc=de \
        cn=grp_heroes -s sub member



.. _ldapsearch_sama_account_mkent:

Searching  sAMAccountName="mkent" and display all attributes
============================================================================

::

    ldapsearch -x -v -h pdc.int.fsf.de \
    -D "<username>" -w "<password>"  \
    -b ou=users,ou="company",dc=int,dc=fsf,dc=de  \
    -s sub "(sAMAccountName=mkent)"


::

    ldap_initialize( ldap://pdc.int.fsf.de )
    filter: (sAMAccountName=mkent)
    requesting: All userApplication attributes

::


    # extended LDIF
    #
    # LDAPv3
    # base <ou=users,ou=company,dc=int,dc=fsf,dc=de> with scope subtree
    # filter: (sAMAccountName=mkent)
    # requesting: ALL
    #

    # Martha Kent, New York, Users, company, int.fsf.de
    dn: CN=Martha Kent,OU=New York,OU=Users,OU=company,DC=int,DC=,dc=de
    objectClass: top
    objectClass: person
    objectClass: organizationalPerson
    objectClass: user
    cn: Martha Kent
    sn: kent
    c: FR
    l: New York
    st:: SXPDqHJl
    description: Compte Utilisateur
    postalCode: 38120
    telephoneNumber: +56 123 85 22 254 341
    facsimileTelephoneNumber: +56 123 85 22 254 342
    givenName: martha
    initials: PVE
    distinguishedName: CN=Martha Kent,OU=New York,OU=Users,OU=fsf,DC=int,DC=fsf,dc=de
    instanceType: 4
    whenCreated: 20170112125817.0Z
    whenChanged: 20190415144530.0Z
    displayName: Martha Kent
    uSNCreated: 196753
    memberOf: CN=grp_heroes,OU=Groups,OU=company,DC=int,DC=fsf,dc=de
    memberOf: CN=super_ops,OU=Groups,OU=company,DC=int,DC=fsf,dc=de
    memberOf: CN=super_admin,OU=Groups,OU=company,DC=int,DC=fsf,dc=de
    memberOf: CN=fsf_all,OU=Groups,OU=company,DC=int,DC=fsf,dc=de
    uSNChanged: 5969820
    department:: U2VydmljZXMgR8OpbsOpcmF1eA==
    company: company
    streetAddress: 5, greenwich village
    wWWHomePage: http://www.fsf.de
    name: Martha Kent
    objectGUID:: s+ejvcXxK0GaaC9WHKsmjg==
    userAccountControl: 512
    badPwdCount: 5
    codePage: 0
    countryCode: 0
    homeDirectory: \\files.srv.int.fsf.de\mkent
    homeDrive: Z:
    badPasswordTime: 131998995971195966
    lastLogoff: 0
    lastLogon: 131995312168156184
    scriptPath: logon.bat
    pwdLastSet: 131484602237872238
    primaryGroupID: 2567
    objectSid:: AQUAAAAAAAUVAAAAWB0ASJ7NFHJC/jn0FwoAAA==
    accountExpires: 9223372036854775807
    logonCount: 3269
    sAMAccountName: mkent
    sAMAccountType: 805306368
    userPrincipalName: mkent@fsf.de
    objectCategory: CN=Person,CN=Schema,CN=Configuration,DC=int,DC=fsf,dc=de
    dSCorePropagationData: 20181109132941.0Z
    dSCorePropagationData: 20171017095326.0Z
    dSCorePropagationData: 16010101000000.0Z
    lastLogonTimestamp: 131998131154488876
    mail: martha.kent@fsf.de

    # search result
    search: 2
    result: 0 Success

    # numResponses: 2
    # numEntries: 1


.. _fsf_users_cn:

Searching  users with "cn" attribute selection
=========================================================

::

    ldapsearch -x -v -h pdc.int.fsf.de \
    -D "<username>" -w "<password>"  \
    -b ou=users,ou="company",dc=int,dc=fsf,dc=de \
    -s sub cn | grep "cn: "


::

    ldap_initialize( ldap://pdc.int.fsf.de )
    filter: (objectclass=*)
    requesting: cn

::

    cn: ....





.. _fsf_attributes_pv:
.. _fsf_ldap_search_pv:

Searching CN="Martha Kent" and display all attributes
======================================================================

::

    ldapsearch -x -v -h pdc.int.fsf.de \
    -D "<username>" -w "<password>"  \
    -b ou=users,ou="company",dc=int,dc=fsf,dc=de  \
    CN="Martha Kent"


or, giving the base research (with the complete path)::

    ldapsearch -x -v -h pdc.int.fsf.de \
    -D "<username>" -w "<password>"  \
    -b cn="Martha Kent",OU=New York,ou=users,ou="company",dc=int,dc=fsf,dc=de


::

    ldap_initialize( ldap://pdc.int.fsf.de )
    filter: CN=Martha Kent
    requesting: All userApplication attributes

::

    # extended LDIF
    #
    # LDAPv3
    # base <ou=users,ou=company,dc=int,dc=fsf,dc=de> with scope subtree
    # filter: CN=Martha Kent
    # requesting: ALL
    #



.. _fsf_ldapsearch_sama_account_sn_mail_pv:

Searching sAMAccountName=mkent  and display sAMAccountName, sn et mail attributes
===========================================================================================

::

    ldapsearch -x -v -h pdc.int.fsf.de \
    -D "<username>" -w "<password>"  \
    -b ou=users,ou="company",dc=int,dc=fsf,dc=de  \
    -s sub "(sAMAccountName=mkent)" \
    -s sub sAMAccountName sn mail

::

    ldap_initialize( ldap://pdc.int.fsf.de )
    filter: (sAMAccountName=mkent)
    requesting: sAMAccountName sn mail
    # extended LDIF
    #
    # LDAPv3
    # base <ou=users,ou=company,dc=int,dc=fsf,dc=de> with scope subtree
    # filter: (sAMAccountName=mkent)
    # requesting: sAMAccountName sn mail
    #

    # Martha Kent, New York, Users, company, int.fsf.de
    dn: CN=Martha Kent,OU=New York,OU=Users,OU=company,DC=int,DC=i
     d3,dc=de
    sn: kent
    sAMAccountName: mkent
    mail: martha.kent@fsf.de

    # search result
    search: 2
    result: 0 Success

    # numResponses: 2
    # numEntries: 1


.. _fsf_attributes_account_sn_mail_pv:
.. _fsf_ldapsearch_account_sn_mail_pv:

Searching CN="Martha Kent" and displaying sAMAccountName, sn and mail
===========================================================================================

::

        ldapsearch -x -v -h pdc.int.fsf.de \
        -D "<username>" -w "<password>"  \
        -b ou=users,ou="company",dc=int,dc=fsf,dc=de  \
        CN="Martha Kent" -s sub sAMAccountName sn mail

::

    ldap_initialize( ldap://pdc.int.fsf.de )
    filter: CN=Martha Kent
    requesting: sAMAccountName sn mail
    # extended LDIF
    #
    # LDAPv3
    # base <ou=users,ou=company,dc=int,dc=fsf,dc=de> with scope subtree
    # filter: CN=Martha Kent
    # requesting: sAMAccountName sn mail
    #

    # Martha Kent, New York, Users, company, int.fsf.de
    dn: CN=Martha Kent,OU=New York,OU=Users,OU=company,DC=int,DC=fsf,dc=de
    sn: kent
    sAMAccountName: mkent
    mail: martha.kent@fsf.de

    # search result
    search: 2
    result: 0 Success

    # numResponses: 2
    # numEntries: 1



.. _fsf_users_accountname:

Searching users with "sAMAccountName" attribute
=================================================

::

    ldapsearch -x -v -h pdc.int.fsf.de \
    -D "<username>" -w "<password>"  \
    -b ou=users,ou="company",dc=int,dc=fsf,dc=de \
    -s sub sAMAccountName | grep "sAMAccountName: "

::

    ldap_initialize( ldap://pdc.int.fsf.de )
    filter: (objectclass=*)
    requesting: sAMAccountName

::


    sAMAccountName: amarchal



.. _fsf_groups_pv:

List of Martha Kent groups
==========================================================

::

    ldapsearch -x -v -h pdc.int.fsf.de \
    -D "<username>" -w "<password>"  \
    -b ou=users,ou="company",dc=int,dc=fsf,dc=de  \
    CN="Martha Kent" -s sub memberOf


::

    ldap_initialize( ldap://pdc.int.fsf.de )
    filter: CN=Martha Kent
    requesting: memberOf

::


    # extended LDIF
    #
    # LDAPv3
    # base <ou=users,ou=company,dc=int,dc=fsf,dc=de> with scope subtree
    # filter: CN=Martha Kent
    # requesting: memberOf
    #

    # Martha Kent, New York, Users, company, int.fsf.de
    dn: CN=Martha Kent,OU=New York,OU=Users,OU=company,DC=int,DC=i
     d3,dc=de
    memberOf: CN=grp_heroes,OU=Groups,OU=company,DC=int,DC=fsf,dc=de
    memberOf: CN=ops,OU=Groups,OU=company,DC=int,DC=fsf,dc=de
    memberOf: CN=log_admin,OU=Groups,OU=company,DC=int,DC=fsf,dc=de
    memberOf: CN=fsf_all,OU=Groups,OU=company,DC=int,DC=fsf,dc=de
    memberOf: CN=fsf_etage,OU=Groups,OU=company,DC=int,DC=fsf,dc=de
    memberOf: CN=ESX Admins,CN=Users,DC=int,DC=fsf,dc=de

    # search result
    search: 2
    result: 0 Success

    # numResponses: 2
    # numEntries: 1
