

.. index::
   pair: Glossary ; LDAP

.. _glossaire_ldap:
.. _ldap_glossary:

========================================
LDAP Glossary
========================================

.. seealso::

   - https://tools.ietf.org/html/rfc4510


.. glossary::



   cn
   common name
   commonName
       .. seealso:: https://tools.ietf.org/html/rfc4519#page-5

       The 'cn' ('commonName' in X.500) attribute type contains names of an
       object.

       Each name is one value of this multi-valued attribute.

       **If the object corresponds to a person, it is typically the
       person's full name**.

       Examples: "Martin K Smith", "Marty Smith" and "printer12".


   DIT
   DIT (Directory Information Tree)
       .. seealso::

          - https://tools.ietf.org/html/rfc4512#page-7

       Les données d'un annuaire LDAP sont organisées en arbre ou DIT
       Cet arbre dispose d'une racine, aussi nommée suffixe puisqu'elle
       termine nécessairement toutes les adresses absolues des objets
       de l'arbre.
       Le DIT est constitué de noeuds et de feuilles que l'on nomme
       DSE (Directory Service entries)


   dc
   domain component
   dc (domainComponent)
       .. seealso::

          - https://tools.ietf.org/html/rfc4519#page-6

       The 'dc' ('domainComponent' in RFC 1274) attribute type is a string
       holding one component, a label, of a DNS domain name
       [RFC1034][RFC2181] naming a host [RFC1123].
       That is, a value of this attribute is a string of ASCII characters
       adhering to the following ABNF [RFC4234]:

       Examples: Valid values include "example" and "com" but not "example.com".
       The latter is invalid as it contains multiple domain components.

   DN
   dn
   DN (Distinguished Name)

       Les DSE sont des aggrégats d'attributs dont certains peuvent posséder
       plusieurs valeurs.
       Elles sont identifiées de façon unique par leur DN qui s'apparente
       à une sorte de *clé primaire*.


   RDN
   rdn
   RDN (Relative Distinguished Names)
       Le nommage des objets LDAP est une science complexe. En effet, les
       options choisies par les éditeurs pour accomplir cette tâche sont,
       malheureusement, très différentes les unes des autres.
       Le :term:`DN` d'un objet est construit par la concaténation de son RDN
       et de celui de ses parents (jusqu'au suffixe du contexte de nommage).

   sn
   surname
       .. seealso::

          - https://tools.ietf.org/html/rfc4519#page-16

       The 'sn' ('surname' in X.500, *nom de famille*) attribute type contains
       name strings for the family names of a person.
       Each string is one value of this multi-valued attribute.

       Example: "Smith".


   ou
   organization unit
   organizationalUnitName
       .. seealso::

          - https://tools.ietf.org/html/rfc4519#page-12

       The 'ou' ('organizationalUnitName' in X.500) attribute type contains
       the names of an organizational unit.  Each name is one value of this
       multi-valued attribute.

       Examples: "Finance", "Human Resources", and "Research and Development".


   uid
   userid
       .. seealso:: https://tools.ietf.org/html/rfc4519#page-18

       The 'uid' ('userid' in RFC 1274) attribute type contains computer
       system login names associated with the object.
       Each name is one value of this multi-valued attribute.

       Examples: "s9709015", "admin", and "Administrator"

   userPassword
       .. seealso:: https://tools.ietf.org/html/rfc4519#page-19

       The 'userPassword' attribute contains octet strings that are known
       only to the user and the system to which the user has access.
       Each string is one value of this multi-valued attribute.

       The application SHOULD prepare textual strings used as passwords by
       transcoding them to Unicode, applying SASLprep [RFC4013], and
       encoding as UTF-8.  The determination of whether a password is
       textual is a local client matter.
