
.. index::
   pair: Awsome ; french-devops
   ! Awsome


.. _awsome:

==========================
Awsome
==========================


.. _awsome_french_devops:

awesome-french-devops (Une liste de liens permettant de se former aux outils utilisés dans le domaine du Devops )
=======================================================================================================================

- https://github.com/stephrobert/awesome-french-devops
- https://github.com/stephrobert/awesome-french-devops/commits.atom
