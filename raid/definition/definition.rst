
.. index::
   pair: Disks ; RAID

.. _raid_def:

==========================
Raid
==========================


Différences entre RAID5/6 et RAID1/10
=========================================

Attention, il ne faut pas sous-estimer les baisses de performance du
Raid5 (et assimilés). Le raid5 ce n'est pas seulement un calcul de
parité, c'est aussi une écriture suplémentaire (ou deux pour le raid6) qui
se fait après le stockage de données elles mêmes (alors que sur du
raid1/10, il n'y a que des écritures simultanées sur deux disque
différents).

Et il ne faut jamais oublier, que quand on écrit un fichier, on n'écrit
pas seulement le contenu du fichier, mais qu'on réécrit en même temps le
fichier virtuel représentant l'entrée de directory, qui peut avoir une
taille assez conséquente pour des boites mails contenant des dizaines de
milliers de messages.

On s'en est très vite aperçu de ces problèmes de performances lorsqu'on
a configuré bender avec un gros raid5 pour y stocker les messages
**ça a été la catastrophe**.

Ce type de raid, c'est donc très bien pour du stockage de gros fichiers
qui ne sont écrits qu'une seule fois et lus beaucoup (par exemple sur un
serveur de contenus vidéos), mais **moins quand on veut écrire rapidement
du contenu, comme quand on fait du Maildir ou du backup...**

C'est probablement pour ça que malgré le gain d'espace disque qu'on peut
obtenir avec du raid5/6, il n'a jamais remplacé le raid1 et ses dérivés...
