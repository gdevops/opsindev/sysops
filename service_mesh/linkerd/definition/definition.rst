

.. _linkerd_def:

=======================================================
linkerd definition
=======================================================

.. seealso::

   - https://github.com/linkerd/linkerd


Linkerd is a transparent service mesh, designed to make modern applications
safe and sane by transparently adding service discovery, load balancing,
failure handling, instrumentation, and routing to all inter-service communication.

Linkerd (pronounced "linker-DEE") acts as a transparent HTTP/gRPC/thrift/etc proxy,
and can usually be dropped into existing applications with a minimum of
configuration, regardless of what language they're written in.

It works with many common protocols and service discovery backends,
including scheduled environments like Mesos and Kubernetes.

Linkerd is built on top of Netty and Finagle, a production-tested RPC
framework used by high-traffic companies like Twitter, Pinterest, Tumblr,
PagerDuty, and others.

Linkerd is hosted by the Cloud Native Computing Foundation (CNCF).
