
.. index::
   pair: Service Mesh ; linkerd
   pair: Service Mesh ; Kubernetes
   ! linkerd

.. _linkerd:

==================================================================================
**linkerd** (pronounce "linker-DEE", A service mesh for Kubernetes and beyond)
==================================================================================

.. seealso::

   - https://github.com/linkerd/linkerd2
   - https://linkerd.io/
   - https://x.com/linkerd
   - https://discourse.linkerd.io/
   - https://www.youtube.com/buoyantio
   - https://landscape.cncf.io/category=service-mesh&format=card-mode&grouping=category&license=open-source&sort=first-commit


.. figure:: cncf_linkerd.png
   :align: center

   https://landscape.cncf.io/category=service-mesh&format=card-mode&grouping=category&license=open-source&selected=linkerd&sort=first-commit

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
