
.. index::
   pair: linkerd; 20.4.2 (2020-04-16)

.. _linkerd_18_9_1:

=======================================================
linkerd 20.4.2 (2020-04-16)
=======================================================

.. seealso::

   - https://github.com/linkerd/linkerd2/tree/edge-20.4.2
   - https://github.com/linkerd/linkerd2/blob/edge-20.4.2/CHANGES.md
