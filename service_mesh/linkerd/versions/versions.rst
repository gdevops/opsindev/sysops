
.. index::
   pair: linkerd; versions

.. _linkerd_versions:

=======================================================
linkerd versions
=======================================================

.. seealso::

   - https://github.com/linkerd/linkerd2/releases

.. toctree::
   :maxdepth: 3

   20.4.2/20.4.2
