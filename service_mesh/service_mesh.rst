
.. index::
   pair: Service ; Mesh
   ! Service mesh

.. _service_mesh:

=======================================================
**Service mesh**
=======================================================

.. seealso::

   - https://landscape.cncf.io/category=service-mesh&format=card-mode&grouping=category&license=open-source&sort=first-commit


.. figure:: cncf_service_mesh.png
   :align: center

   https://landscape.cncf.io/category=service-mesh&format=card-mode&grouping=category&license=open-source&sort=first-commit


.. toctree::
   :maxdepth: 3

   linkerd/linkerd
