
.. index::
   pair: OCS ; Inventory
   ! OCSInventory NG

.. _ocs_inventory_ng:

==========================
OCS Inventory NG
==========================

.. seealso::

   - https://github.com/OCSInventory-NG
   - https://fr.wikipedia.org/wiki/OCS_Inventory
   - http://wiki.ocsinventory-ng.org/

.. figure:: ocs-petit.png
   :align: center
   :width: 300


.. toctree::
   :maxdepth: 6

   server/server
   agents/agents
