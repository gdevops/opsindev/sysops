
.. index::
   pair: OCS ; Unix agent

.. _ocs_unix_agent:

==============================
OCS Inventory NG Unix agent
==============================

.. seealso::

   - https://github.com/OCSInventory-NG


.. toctree::
   :maxdepth: 3

   requirements/requirements
   versions/versions
