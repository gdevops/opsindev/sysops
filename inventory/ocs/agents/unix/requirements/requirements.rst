

.. _ocs_unix_agent_requirements:

=========================================
OCS Inventory NG Unix agent requirements
=========================================

.. seealso::

   - https://github.com/OCSInventory-NG

The following commands are needed:

- dmidecode on Linux and *BSD* (i386, amd64, ia64) => dmidecode is required to
  read the BIOS stats.
- lspci on Linux and *BSD* (pciutils package) => lspci is required to list
  PCI devices.
- sneep on Solaris/sparc, you must install sneep and record the Serial Number
  with it (download it from http://www.sun.com/download/products.xml?id=4304155a)
- To get the serial number of the screen you will need one of these tools:

  - monitor-edid from Mandriva is needed to fetch the monitor.
    A package is available in Fedora repository
    information http://wiki.mandriva.com/en/Tools/monitor-edid
  - get-edid from the read-edid package

- ipmitool if you want to collect information about IPMI
- Nmap (v3.90 or superior) to scan network devices for Ipdiscover
