

.. _ocs_unix_agent_versions:

======================================
OCS Inventory NG Unix agent versions
======================================

.. seealso::

   - https://github.com/OCSInventory-NG/UnixAgent/releases

Contributing
==============

- Fork it!
- Create your feature branch: git checkout -b my-new-feature
- Add your changes: git add folder/file1.php
- Commit your changes: git commit -m 'Add some feature'
- Push to the branch: git push origin my-new-feature
- Submit a pull request !


.. toctree::
   :maxdepth: 3

   2.4.2/2.4.2
