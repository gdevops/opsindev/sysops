
.. index::
   pair: OCS ; agents

.. _ocs_agents:

==========================
OCS Inventory NG agents
==========================

.. seealso::

   - https://github.com/OCSInventory-NG


.. toctree::
   :maxdepth: 3

   unix/unix
