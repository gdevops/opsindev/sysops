
.. index::
   pair: OCS ; Inventory
   ! OCSInventory server

.. _ocs_inventory_server:

==========================
OCS Inventory server
==========================

.. seealso::

   - https://fr.wikipedia.org/wiki/OCS_Inventory
   - https://github.com/OCSInventory-NG/OCSInventory-Server
   - http://wiki.ocsinventory-ng.org/


.. toctree::
   :maxdepth: 3

   versions/versions
