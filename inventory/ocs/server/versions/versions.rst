
.. index::
   pair: OCS ; Versions

.. _ocs_inventory_versions:

==========================
OCS Inventory versions
==========================

.. seealso::


   - https://github.com/OCSInventory-NG/OCSInventory-Server/releases

.. toctree::
   :maxdepth: 3

   2.6/2.6
