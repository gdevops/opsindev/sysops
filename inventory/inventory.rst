
.. index::
   pair: Software ; Inventory
   ! Inventory

.. _inventory:

==========================
Inventory
==========================

.. toctree::
   :maxdepth: 5

   inxi/inxi
   ocs/ocs
