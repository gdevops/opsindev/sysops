
.. index::
   pair: HAProxy; prometheus exporter


.. _haproxy_prometheus_exporter:
.. _promex:

===========================================
PROMEX: A Prometheus exporter for HAProxy
===========================================

.. seealso::

   - https://github.com/haproxy/haproxy/tree/master/contrib/prometheus-exporter
   - https://git.haproxy.org/?p=haproxy.git;a=commitdiff;h=f959d0809ead017555ef5db60ef081a7fd87ca20
   - https://github.com/prometheus/haproxy\_exporter

.. include:: README.rst
