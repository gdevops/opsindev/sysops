PROMEX: A Prometheus exporter for HAProxy
-----------------------------------------

Prometheus is a monitoring and alerting system.
More and more people use it to monitor their environment (this is written February 2019).
It collects metrics from monitored targets by scraping metrics HTTP endpoints
on these targets.

For HAProxy, The Prometheus team offically supports an exporter written in :ref:Go <go_language>`
(https://github.com/prometheus/haproxy\_exporter).

But it requires an extra software to deploy and monitor.

PROMEX, on its side, is a built-in Prometheus exporter for HAProxy.

It was developed as a service and is directly available in HAProxy, like the
stats applet.

However, PROMEX is not built by default with HAProxy. It is provided as
an extra component for everyone want to use it.
So you need to explicity build HAProxy with the PROMEX service, using the
Makefile variable "EXTRA\_OBJS". For instance:

::

    > make TARGET=linux2628 EXTRA_OBJS="contrib/prometheus-exporter/service-prometheus.o"

if HAProxy provides the PROMEX service, the following build option will
be reported by the command "haproxy -vv"::

    Built with the Prometheus exporter as a service

To be used, it must be enabled in the configuration with an
"http-request" rule and the corresponding HTTP proxy must enable the HTX
support. For instance:

::

    frontend test
        mode http
        ...
        option http-use-htx
        http-request use-service prometheus-exporter if { path /metrics }
        ...

This service has been developed as a third-party component because it
could become obsolete, depending on how much time Prometheus will remain
heavily used. This is said with no ulterior motive of course. Prometheus
is a great software and I hope all the well for it. But we involve in a
environment moving quickly and a solution may be obvious today could be
deprecated the next year. And because PROMEX is not integrated by
default into the HAProxy codebase, it will need some interest to be
actively supported. All contribution of any kind are welcome.

You must also be careful if you use with huge configurations. Unlike the
stats applet, all metrics are not grouped by service (proxy, listener or
server). With PROMEX, all lines for a given metric are provided as one
single group. So instead of collecting all metrics for a proxy before
moving to the next one, we must loop on all proxies for each metric.
Same for the servers. Thus, it will spend much more ressources to
produce the Prometheus metrics than the CSV export through the stats
page. To give a comparison order, quick benchmarks shown that a PROMEX
dump is 5x slower and 20x more verbose than a CSV export.

Exported metrics
----------------

-  Globals metrics

+-----------------------------+----------------------------------------------+
| Metric name                 | Description                                  |
+-----------------------------+----------------------------------------------+
| haproxy\_process\_nbthread  | Configured number of threads. Configured     |
| haproxy\_process\_nbproc    | number of processes. Relative process id,    |
| haproxy\_process\_relative\ | starting at 1. Uptime in seconds.            |
| _process\_id                | Per-process memory limit (in MB); 0=unset.   |
| haproxy\_process\_uptime\_s | Total amount of memory allocated in pools    |
| econds                      | (in MB). Total amount of memory used in      |
| haproxy\_process\_max\_memo | pools (in MB). Total number of failed pool   |
| ry                          | allocations. Maximum number of open file     |
| haproxy\_process\_pool\_all | descriptors; 0=unset. Maximum numer of open  |
| ocated\_total               | sockets. Maximum number of concurrent        |
| haproxy\_process\_pool\_use | connections. Initial Maximum number of       |
| d\_total                    | concurrent connections. Number of active     |
| haproxy\_process\_pool\_fai | sessions. Total number of terminated         |
| lures\_total                | sessions. Total number of requests (TCP or   |
| haproxy\_process\_max\_fds  | HTTP). Configured maximum number of          |
| haproxy\_process\_max\_sock | concurrent SSL connections. Number of opened |
| ets                         | SSL connections. Total number of opened SSL  |
| haproxy\_process\_max\_conn | connections. Configured maximum number of    |
| ections                     | pipes. Number of pipes in used. Number of    |
| haproxy\_process\_hard\_max | pipes unused. Current number of connections  |
| \_connections               | per second over last elapsed second.         |
| haproxy\_process\_current\_ | Configured maximum number of connections per |
| connections                 | second. Maximum observed number of           |
| haproxy\_process\_connectio | connections per second. Current number of    |
| ns\_total                   | sessions per second over last elapsed        |
| haproxy\_process\_requests\ | second. Configured maximum number of         |
| _total                      | sessions per second. Maximum observed number |
| haproxy\_process\_max\_ssl\ | of sessions per second. Current number of    |
| _connections                | SSL sessions per second over last elapsed    |
| haproxy\_process\_current\_ | second. Configured maximum number of SSL     |
| ssl\_connections            | sessions per second. Maximum observed number |
| haproxy\_process\_ssl\_conn | of SSL sessions per second. Current frontend |
| ections\_total              | SSL Key computation per second over last     |
| haproxy\_process\_max\_pipe | elapsed second. Maximum observed frontend    |
| s                           | SSL Key computation per second. SSL session  |
| haproxy\_process\_pipes\_us | reuse ratio (percent). Current backend SSL   |
| ed\_total                   | Key computation per second over last elapsed |
| haproxy\_process\_pipes\_fr | second. Maximum observed backend SSL Key     |
| ee\_total                   | computation per second. Total number of SSL  |
| haproxy\_process\_current\_ | session cache lookups. Total number of SSL   |
| connection\_rate            | session cache misses. Number of bytes per    |
| haproxy\_process\_limit\_co | second over last elapsed second, before http |
| nnection\_rate              | compression. Number of bytes per second over |
| haproxy\_process\_max\_conn | last elapsed second, after http compression. |
| ection\_rate                | Configured maximum input compression rate in |
| haproxy\_process\_current\_ | bytes. Current memory used for zlib in       |
| session\_rate               | bytes. Configured maximum amount of memory   |
| haproxy\_process\_limit\_se | for zlib in bytes. Current number of tasks.  |
| ssion\_rate                 | Current number of tasks in the run-queue.    |
| haproxy\_process\_max\_sess | Idle to total ratio over last sample         |
| ion\_rate                   | (percent). Non zero means stopping in        |
| haproxy\_process\_current\_ | progress. Current number of active jobs      |
| ssl\_rate                   | (listeners, sessions, open devices). Current |
| haproxy\_process\_limit\_ss | number of active jobs that can't be stopped  |
| l\_rate                     | during a soft stop. Current number of active |
| haproxy\_process\_max\_ssl\ | listeners. Current number of active peers.   |
| _rate                       | Current number of connected peers. Total     |
| haproxy\_process\_current\_ | number of dropped logs. Non zero if the busy |
| frontend\_ssl\_key\_rate    | polling is enabled.                          |
| haproxy\_process\_max\_fron |                                              |
| tend\_ssl\_key\_rate        |                                              |
| haproxy\_process\_frontent\ |                                              |
| _ssl\_reuse                 |                                              |
| haproxy\_process\_current\_ |                                              |
| backend\_ssl\_key\_rate     |                                              |
| haproxy\_process\_max\_back |                                              |
| end\_ssl\_key\_rate         |                                              |
| haproxy\_process\_ssl\_cach |                                              |
| e\_lookups                  |                                              |
| haproxy\_process\_ssl\_cach |                                              |
| e\_misses                   |                                              |
| haproxy\_process\_http\_com |                                              |
| p\_bytes\_in\_total         |                                              |
| haproxy\_process\_http\_com |                                              |
| p\_bytes\_out\_total        |                                              |
| haproxy\_process\_limit\_ht |                                              |
| tp\_comp                    |                                              |
| haproxy\_process\_current\_ |                                              |
| zlib\_memory                |                                              |
| haproxy\_process\_max\_zlib |                                              |
| \_memory                    |                                              |
| haproxy\_process\_current\_ |                                              |
| tasks                       |                                              |
| haproxy\_process\_current\_ |                                              |
| run\_queue                  |                                              |
| haproxy\_process\_idle\_tim |                                              |
| e\_percent                  |                                              |
| haproxy\_process\_stopping  |                                              |
| haproxy\_process\_jobs      |                                              |
| haproxy\_process\_unstoppab |                                              |
| le\_jobs                    |                                              |
| haproxy\_process\_listeners |                                              |
| haproxy\_process\_active\_p |                                              |
| eers                        |                                              |
| haproxy\_process\_connected |                                              |
| \_peers                     |                                              |
| haproxy\_process\_dropped\_ |                                              |
| logs\_total                 |                                              |
| haproxy\_process\_busy\_pol |                                              |
| ling\_enabled               |                                              |
+-----------------------------+----------------------------------------------+

-  Frontend metrics

+-----------------------------+----------------------------------------------+
| Metric name                 | Description                                  |
+-----------------------------+----------------------------------------------+
| haproxy\_frontend\_status   | Current status of the service. Current       |
| haproxy\_frontend\_current\ | number of active sessions. Maximum observed  |
| _sessions                   | number of active sessions. Configured        |
| haproxy\_frontend\_max\_ses | session limit. Total number of sessions.     |
| sions                       | Current number of sessions per second over   |
| haproxy\_frontend\_limit\_s | last elapsed second. Configured limit on new |
| essions                     | sessions per second. Maximum observed number |
| haproxy\_frontend\_sessions | of sessions per second. Current number of    |
| \_total                     | connections per second over the last elapsed |
| haproxy\_frontend\_current\ | second. Maximum observed number of           |
| _session\_rate              | connections per second. Total number of      |
| haproxy\_frontend\_limit\_s | connections. Current total of incoming       |
| ession\_rate                | bytes. Current total of outgoing bytes.      |
| haproxy\_frontend\_max\_ses | Total number of denied requests. Total       |
| sion\_rate                  | number of denied responses. Total number of  |
| haproxy\_frontend\_connecti | request errors. Total number of requests     |
| ons\_rate\_current          | denied by "tcp-request connection" rules.    |
| haproxy\_frontend\_connecti | Total number of requests denied by           |
| ons\_rate\_max              | "tcp-request session" rules. Total number of |
| haproxy\_frontend\_connecti | failed header rewriting warnings. Current    |
| ons\_total                  | number of HTTP requests per second over last |
| haproxy\_frontend\_bytes\_i | elapsed second. Maximum observed number of   |
| n\_total                    | HTTP requests per second. Total number of    |
| haproxy\_frontend\_bytes\_o | HTTP requests received. Total number of HTTP |
| ut\_total                   | responses. Total number of intercepted HTTP  |
| haproxy\_frontend\_requests | requests. Total number of HTTP cache         |
| \_denied\_total             | lookups. Total number of HTTP cache hits.    |
| haproxy\_frontend\_response | Total number of HTTP response bytes fed to   |
| s\_denied\_total            | the compressor. Total number of HTTP         |
| haproxy\_frontend\_request\ | response bytes emitted by the compressor.    |
| _errors\_total              | Total number of bytes that bypassed the HTTP |
| haproxy\_frontend\_denied\_ | compressor (CPU/BW limit). Total number of   |
| connections\_total          | HTTP responses that were compressed.         |
| haproxy\_frontend\_denied\_ |                                              |
| sessions\_total             |                                              |
| haproxy\_frontend\_failed\_ |                                              |
| header\_rewriting\_total    |                                              |
| haproxy\_frontend\_http\_re |                                              |
| quests\_rate\_current       |                                              |
| haproxy\_frontend\_http\_re |                                              |
| quests\_rate\_max           |                                              |
| haproxy\_frontend\_http\_re |                                              |
| quests\_total               |                                              |
| haproxy\_frontend\_http\_re |                                              |
| sponses\_total              |                                              |
| haproxy\_frontend\_intercep |                                              |
| ted\_requests\_total        |                                              |
| haproxy\_frontend\_http\_ca |                                              |
| che\_lookups\_total         |                                              |
| haproxy\_frontend\_http\_ca |                                              |
| che\_hits\_total            |                                              |
| haproxy\_frontend\_http\_co |                                              |
| mp\_bytes\_in\_total        |                                              |
| haproxy\_frontend\_http\_co |                                              |
| mp\_bytes\_out\_total       |                                              |
| haproxy\_frontend\_http\_co |                                              |
| mp\_bytes\_bypassed\_total  |                                              |
| haproxy\_frontend\_http\_co |                                              |
| mp\_responses\_total        |                                              |
+-----------------------------+----------------------------------------------+

-  Backend metrics

+--------------------------------+-------------------------------------------+
| Metric name                    | Description                               |
+--------------------------------+-------------------------------------------+
| haproxy\_backend\_status       | Current status of the service. Current    |
| haproxy\_backend\_current\_ses | number of active sessions. Maximum        |
| sions                          | observed number of active sessions.       |
| haproxy\_backend\_max\_session | Configured session limit. Total number of |
| s                              | sessions. Current number of sessions per  |
| haproxy\_backend\_limit\_sessi | second over last elapsed second. Maximum  |
| ons                            | observed number of sessions per second.   |
| haproxy\_backend\_sessions\_to | Number of seconds since last session      |
| tal                            | assigned to server/backend. Current       |
| haproxy\_backend\_current\_ses | number of queued requests. Maximum        |
| sion\_rate                     | observed number of queued requests. Total |
| haproxy\_backend\_max\_session | number of connection establishment        |
| \_rate                         | attempts. Total number of connection      |
| haproxy\_backend\_last\_sessio | reuses. Current total of incoming bytes.  |
| n\_seconds                     | Current total of outgoing bytes. Avg.     |
| haproxy\_backend\_current\_que | queue time for last 1024 successful       |
| ue                             | connections. Avg. connect time for last   |
| haproxy\_backend\_max\_queue   | 1024 successful connections. Avg.         |
| haproxy\_backend\_connection\_ | response time for last 1024 successful    |
| attempts\_total                | connections. Avg. total time for last     |
| haproxy\_backend\_connection\_ | 1024 successful connections. Total number |
| reuses\_total                  | of denied requests. Total number of       |
| haproxy\_backend\_bytes\_in\_t | denied responses. Total number of         |
| otal                           | connection errors. Total number of        |
| haproxy\_backend\_bytes\_out\_ | response errors. Total number of retry    |
| total                          | warnings. Total number of redispatch      |
| haproxy\_backend\_http\_queue\ | warnings. Total number of failed header   |
| _time\_average\_seconds        | rewriting warnings. Total number of data  |
| haproxy\_backend\_http\_connec | transfers aborted by the client. Total    |
| t\_time\_average\_seconds      | number of data transfers aborted by the   |
| haproxy\_backend\_http\_respon | server. Service weight. Current number of |
| se\_time\_average\_seconds     | active servers. Current number of backup  |
| haproxy\_backend\_http\_total\ | servers. Total number of UP->DOWN         |
| _time\_average\_seconds        | transitions. Number of seconds since the  |
| haproxy\_backend\_requests\_de | last UP<->DOWN transition. Total downtime |
| nied\_total                    | (in seconds) for the service. Total       |
| haproxy\_backend\_responses\_d | number of times a service was selected.   |
| enied\_total                   | Total number of HTTP requests received.   |
| haproxy\_backend\_connection\_ | Total number of HTTP responses. Total     |
| errors\_total                  | number of HTTP cache lookups. Total       |
| haproxy\_backend\_response\_er | number of HTTP cache hits. Total number   |
| rors\_total                    | of HTTP response bytes fed to the         |
| haproxy\_backend\_retry\_warni | compressor. Total number of HTTP response |
| ngs\_total                     | bytes emitted by the compressor. Total    |
| haproxy\_backend\_redispatch\_ | number of bytes that bypassed the HTTP    |
| warnings\_total                | compressor (CPU/BW limit). Total number   |
| haproxy\_backend\_failed\_head | of HTTP responses that were compressed.   |
| er\_rewriting\_total           |                                           |
| haproxy\_backend\_client\_abor |                                           |
| ts\_total                      |                                           |
| haproxy\_backend\_server\_abor |                                           |
| ts\_total                      |                                           |
| haproxy\_backend\_weight       |                                           |
| haproxy\_backend\_active\_serv |                                           |
| ers                            |                                           |
| haproxy\_backend\_backup\_serv |                                           |
| ers                            |                                           |
| haproxy\_backend\_check\_up\_d |                                           |
| own\_total                     |                                           |
| haproxy\_backend\_check\_last\ |                                           |
| _change\_seconds               |                                           |
| haproxy\_backend\_downtime\_se |                                           |
| conds\_total                   |                                           |
| haproxy\_backend\_loadbalanced |                                           |
| \_total                        |                                           |
| haproxy\_backend\_http\_reques |                                           |
| ts\_total                      |                                           |
| haproxy\_backend\_http\_respon |                                           |
| ses\_total                     |                                           |
| haproxy\_backend\_http\_cache\ |                                           |
| _lookups\_total                |                                           |
| haproxy\_backend\_http\_cache\ |                                           |
| _hits\_total                   |                                           |
| haproxy\_backend\_http\_comp\_ |                                           |
| bytes\_in\_total               |                                           |
| haproxy\_backend\_http\_comp\_ |                                           |
| bytes\_out\_total              |                                           |
| haproxy\_backend\_http\_comp\_ |                                           |
| bytes\_bypassed\_total         |                                           |
| haproxy\_backend\_http\_comp\_ |                                           |
| responses\_total               |                                           |
+--------------------------------+-------------------------------------------+

-  Server metrics

+-------------------------------+--------------------------------------------+
| Metric name                   | Description                                |
+-------------------------------+--------------------------------------------+
| haproxy\_server\_status       | Current status of the service. Current     |
| haproxy\_server\_current\_ses | number of active sessions. Maximum         |
| sions                         | observed number of active sessions.        |
| haproxy\_server\_max\_session | Configured session limit. Total number of  |
| s                             | sessions. Current number of sessions per   |
| haproxy\_server\_limit\_sessi | second over last elapsed second. Maximum   |
| ons                           | observed number of sessions per second.    |
| haproxy\_server\_sessions\_to | Number of seconds since last session       |
| tal                           | assigned to server/backend. Current number |
| haproxy\_server\_current\_ses | of queued requests. Maximum observed       |
| sion\_rate                    | number of queued requests. Configured      |
| haproxy\_server\_max\_session | maxqueue for the server (0 meaning no      |
| \_rate                        | limit). Current total of incoming bytes.   |
| haproxy\_server\_last\_sessio | Current total of outgoing bytes. Avg.      |
| n\_seconds                    | queue time for last 1024 successful        |
| haproxy\_server\_current\_que | connections. Avg. connect time for last    |
| ue                            | 1024 successful connections. Avg. response |
| haproxy\_server\_max\_queue   | time for last 1024 successful connections. |
| haproxy\_server\_queue\_limit | Avg. total time for last 1024 successful   |
| haproxy\_server\_bytes\_in\_t | connections. Total number of connection    |
| otal                          | establishment attempts. Total number of    |
| haproxy\_server\_bytes\_out\_ | connection reuses. Total number of denied  |
| total                         | responses. Total number of connection      |
| haproxy\_server\_http\_queue\ | errors. Total number of response errors.   |
| _time\_average\_seconds       | Total number of retry warnings. Total      |
| haproxy\_server\_http\_connec | number of redispatch warnings. Total       |
| t\_time\_average\_seconds     | number of failed header rewriting          |
| haproxy\_server\_http\_respon | warnings. Total number of data transfers   |
| se\_time\_average\_seconds    | aborted by the client. Total number of     |
| haproxy\_server\_http\_total\ | data transfers aborted by the server.      |
| _time\_average\_seconds       | Service weight. Total number of failed     |
| haproxy\_server\_connection\_ | check (Only when the server is up). Total  |
| attempts\_total               | number of UP->DOWN transitions. Total      |
| haproxy\_server\_connection\_ | downtime (in seconds) for the service.     |
| reuses\_total                 | Number of seconds since the last UP<->DOWN |
| haproxy\_server\_responses\_d | transition. Current throttle percentage    |
| enied\_total                  | for the server, when slowstart is active.  |
| haproxy\_server\_connection\_ | Total number of times a service was        |
| errors\_total                 | selected. Total number of HTTP responses.  |
| haproxy\_server\_response\_er |                                            |
| rors\_total                   |                                            |
| haproxy\_server\_retry\_warni |                                            |
| ngs\_total                    |                                            |
| haproxy\_server\_redispatch\_ |                                            |
| warnings\_total               |                                            |
| haproxy\_server\_failed\_head |                                            |
| er\_rewriting\_total          |                                            |
| haproxy\_server\_client\_abor |                                            |
| ts\_total                     |                                            |
| haproxy\_server\_server\_abor |                                            |
| ts\_total                     |                                            |
| haproxy\_server\_weight       |                                            |
| haproxy\_server\_check\_failu |                                            |
| res\_total                    |                                            |
| haproxy\_server\_check\_up\_d |                                            |
| own\_total                    |                                            |
| haproxy\_server\_downtime\_se |                                            |
| conds\_total                  |                                            |
| haproxy\_server\_check\_last\ |                                            |
| _change\_seconds              |                                            |
| haproxy\_server\_current\_thr |                                            |
| ottle                         |                                            |
| haproxy\_server\_loadbalanced |                                            |
| \_total                       |                                            |
| haproxy\_server\_http\_respon |                                            |
| ses\_total                    |                                            |
+-------------------------------+--------------------------------------------+
