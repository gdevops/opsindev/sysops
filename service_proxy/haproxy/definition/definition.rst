

.. _haproxy_def:

=================================
HAProxy definition
=================================

.. contents::
   :depth: 3

Wikipedia definition
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/HAProxy
   - https://git.haproxy.org/?p=haproxy.git

HAProxy is free, open source software that provides a high availability
load balancer and proxy server for TCP and HTTP-based applications that
spreads requests across multiple servers.

It is written in C and has a reputation for being fast and efficient
(in terms of processor and memory usage).

HAProxy is used by a number of high-profile websites including GoDaddy,
GitHub, Bitbucket,[4] Stack Overflow,[5] Reddit, Speedtest.net, Tumblr,
Twitter[6][7] and Tuenti[8] and is used in the OpsWorks product from
Amazon Web Services.[9]
