
.. index::
   pair: HAProxy; Versions


.. _haproxy_versions:

=================================
HAProxy versions
=================================

.. seealso::

   - https://github.com/haproxy/haproxy/releases

.. toctree::
   :maxdepth: 3

   2.1.0/2.1.0
   2.0.0/2.0.0
   1.9.0/1.9.0
