
.. index::
   pair: Load Balancing; HAProxy
   ! Willy Tarreau
   ! HAProxy

.. _haproxy:

=================================================================
HAProxy, The Reliable, High Performance TCP/HTTP Load Balancer
=================================================================

.. seealso::

   - https://en.wikipedia.org/wiki/HAProxy
   - https://git.haproxy.org/?p=haproxy.git
   - https://github.com/haproxy/haproxy
   - https://x.com/haproxy
   - https://www.haproxy.org/
   - http://1wt.eu/#wami (Willy Tarreau)


.. figure:: cncf_haproxy.png
   :align: center

   https://landscape.cncf.io/category=service-proxy&format=card-mode&grouping=category&selected=ha-proxy


.. figure:: haproxy_logo.jpg
   :align: center

.. toctree::
   :maxdepth: 6

   definition/definition
   contrib/contrib
   news/news
   versions/versions
