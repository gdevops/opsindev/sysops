
.. index::
   pair: Load Balancing ; Definition

.. _load_balancing_def:

==========================
Load balancing definition
==========================

.. contents::
   :depth: 3

Wikipedia definition
=====================

.. seealso::

   - https://fr.wikipedia.org/wiki/Ing%C3%A9nierie_de_fiabilit%C3%A9


In computing, load balancing improves the distribution of workloads
across multiple computing resources, such as computers, a computer
cluster, network links, central processing units, or disk drives.

Load balancing aims to optimize resource use, maximize throughput,
minimize response time, and avoid overload of any single resource.

Using multiple components with load balancing instead of a single
component may increase reliability and availability through redundancy.

Load balancing usually involves dedicated software or hardware, such as
a multilayer switch or a Domain Name System server process.


French Wikipedia definition
============================

.. seealso::

   - https://fr.wikipedia.org/wiki/R%C3%A9partition_de_charge


En informatique, la répartition de charge (en anglais : load balancing)
est un ensemble de techniques permettant de distribuer une charge de
travail entre différents ordinateurs d'un groupe.

Ces techniques permettent à la fois de répondre à une charge trop
importante d'un service en la répartissant sur plusieurs serveurs,
et de réduire l'indisponibilité potentielle de ce service que pourrait
provoquer la panne logicielle ou matérielle d'un unique serveur.

Ces techniques sont par exemple très utilisées dans le domaine des
services HTTP où un site à forte audience doit pouvoir gérer des
centaines de milliers de requêtes par seconde.

La répartition de charge est issue de la recherche dans le domaine des
ordinateurs parallèles.

L'architecture la plus courante est constituée de plusieurs répartiteurs
de charge (genres de routeurs dédiés à cette tâche), un principal, et
un ou plusieurs de secours pouvant prendre le relais, et d'une collection
d'ordinateurs similaires effectuant les calculs.

On peut appeler cet ensemble de serveurs une ferme de serveurs (anglais
server farm) ou de façon plus générique, une grappe de serveurs
(anglais server cluster).

On parle encore de server pool (littéralement, « groupe de serveurs »).
