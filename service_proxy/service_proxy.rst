
.. index::
   pair: Service ; Proxy
   ! Service Proxy

.. _service_Proxy:

=======================================================
**Service Proxy**
=======================================================

.. seealso::

   - https://landscape.cncf.io/category=service-proxy&format=card-mode&grouping=category
   - https://fr.wikipedia.org/wiki/Ing%C3%A9nierie_de_fiabilit%C3%A9
   - https://en.wikipedia.org/wiki/Load_balancing_(computing)


.. figure:: cncf_service_proxy.png
   :align: center

   https://landscape.cncf.io/category=service-proxy&format=card-mode&grouping=category


.. toctree::
   :maxdepth: 6

   definition/definition
   haproxy/haproxy
   nginx/nginx
   traefik/traefik
