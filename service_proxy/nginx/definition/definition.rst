

.. _nginx_def:
.. _nginx_balancer_def:

=================================
Nginx definition
=================================

.. contents::
   :depth: 3

Wikipedia definition
=====================

.. seealso::

   - https://en.wikipedia.org/wiki/Nginx

Nginx (/ˌɛndʒɪnˈɛks/ EN-jin-EKS) (stylized as NGINX or nginx) is a
web server which can also be used as a reverse proxy, load balancer,
mail proxy and HTTP cache.

The software was created by Igor Sysoev and first publicly released in
2004. A company of the same name was founded in 2011 to provide support
and Nginx plus paid software.

Nginx is free and open-source software, released under the terms of a
BSD-like license.

A large fraction of web servers use NGINX, often as a load balancer.

In March 2019, the Nginx company was acquired by `F5 Networks`_ for
$670 million.


.. _`F5 Networks`:  https://en.wikipedia.org/wiki/F5_Networks
