
.. index::
   pair: Load Balancing; Nginx
   ! Nginx

.. _nginx_load_balancer:

============================================
Nginx /ɛndʒɪnˈɛks/ EN-jin-EKS load balancer
============================================

.. seealso::

   - https://en.wikipedia.org/wiki/Nginx
   - https://github.com/nginx/nginx
   - https://x.com/nginx
   - https://nginx.org/
   - :ref:`nginx_http_server`


.. figure:: Nginx_logo.svg.png
   :align: center

.. figure:: cncf_nginx.png
   :align: center

   https://landscape.cncf.io/category=service-proxy&format=card-mode&grouping=category&selected=nginx


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
