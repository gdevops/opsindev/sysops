
.. index::
   pair: Versions; Traefik
   ! Traefik

.. _traefik_versions:

============================================
Traefik versions
============================================

.. seealso::

   - https://github.com/containous/traefik/releases

.. toctree::
   :maxdepth: 3

   2.2.0/2.2.0
   2.0.0/2.0.0
   1.7.11/1.7.11
