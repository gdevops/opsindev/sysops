
.. index::
   pair: Traefik ; Load Balancing
   ! Traefik

.. _traefik:

==========================================
Traefik (The Cloud Native Edge Router)
==========================================

.. seealso::

   - https://github.com/containous/traefik
   - https://traefik.io/
   - https://x.com/containous
   - https://x.com/emilevauge
   - https://blog.containo.us/back-to-traefik-2-0-2f9aa17be305
   - https://blog.containo.us/traefikee-now-dockeree-certified-d926bf7255a4


.. figure:: traefik.logo.svg
   :align: center
   :width: 300

.. figure:: cncf_traefik.png
   :align: center

   https://landscape.cncf.io/category=service-proxy&format=card-mode&grouping=category&selected=traefik


.. toctree::
   :maxdepth: 6

   versions/versions
