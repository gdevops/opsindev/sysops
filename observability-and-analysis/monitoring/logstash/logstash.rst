
.. index::
   pair: Logs; logstash

.. _logstash:

===============================================================================
logstash (Logstash - transport and process your logs, events, or other data)
===============================================================================

.. seealso::

   - https://github.com/elastic/logstash
   - https://www.elastic.co/fr/logstash
   - https://x.com/elastic
   - https://fr.wikipedia.org/wiki/Logstash
   - https://www.elastic.co/fr/blog/feed
   - https://landscape.cncf.io/category=logging&format=card-mode&grouping=category&selected=logstash


.. figure:: icon-logstash-bb.svg
   :align: center

   logstash icon


.. figure:: cncf_logstash.png
   :align: center

   https://landscape.cncf.io/category=logging&format=card-mode&grouping=category&selected=logstash


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
