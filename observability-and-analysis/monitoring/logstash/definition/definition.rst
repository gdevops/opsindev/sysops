
.. index::
   pair: Definition; logstash

.. _logstash_def:

==========================
logstash definition
==========================

.. contents::
   :depth: 3

French wikipedia definition
=============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Logstash

Logstash est un outil informatique de collecte, analyse et stockage
de logs_.

Il est généralement associé avec Elasticsearch_, moteur de recherche
distribué, et Kibana_, interface d'Elasticsearch.

Logstash est capable d'intégrer une multitude de sources simultanément


.. _logs:  https://fr.wikipedia.org/wiki/Historique_(informatique)
.. _Elasticsearch: https://fr.wikipedia.org/wiki/Elasticsearch
.. _Kibana: https://fr.wikipedia.org/wiki/Kibana

Description
------------

Logstash peut être considéré comme un ETL (Extract-transform-load).

Il permet de centraliser les différentes traces et d'en faire une
analyse efficace.
Il est capable de gérer pratiquement tous les types de logs: journaux
du système, journaux du serveur Web, journaux d'erreurs et journaux
des applications.
Il se positionne côté serveur, et est sous licence open source
(licence Apache).

Il permet de filtrer les messages, d'extraire des informations utiles et
de les stocker pour les indexer (typiquement dans Elasticsearch).

Il peut être intégré à ArcSight, système de gestion de sécurité.

La pile Elastic dont fait partie Logstash est un des principaux
concurrents de splunk.
