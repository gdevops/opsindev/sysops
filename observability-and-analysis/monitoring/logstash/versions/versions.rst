
.. index::
   pair: logstash; versions

.. _logstash_versions:

===============================================================================
logstash versions
===============================================================================

.. seealso::

   - https://github.com/elastic/logstash/releases

.. toctree::
   :maxdepth: 3

   7.6.2/7.6.2
