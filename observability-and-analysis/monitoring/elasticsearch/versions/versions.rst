

.. _elasticsearch_versions:

=======================================================================
Elasticsearch versions
=======================================================================

.. seealso::

   - https://github.com/elastic/elasticsearch/releases

.. toctree::
   :maxdepth: 3

   7.6.2/7.6.2
   7.6.0/7.6.0
