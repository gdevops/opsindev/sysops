
.. index::
   pair: Logs; elastic
   ! Elastic

.. _elasticsearch:
.. _elastic:

=======================================================================
**Elasticsearch** (Open Source, Distributed, RESTful Search Engine)
=======================================================================

.. seealso::

   - https://www.elastic.co/fr/elasticsearch
   - https://fr.wikipedia.org/wiki/Elasticsearch
   - https://github.com/elastic/elasticsearch
   - https://landscape.cncf.io/category=logging&format=card-mode&grouping=category
   - https://landscape.cncf.io/category=logging&format=card-mode&grouping=category&selected=elastic


.. figure:: elastic.svg
   :align: center


.. figure:: lien_cncf.png
   :align: center

   https://landscape.cncf.io/category=logging&format=card-mode&grouping=category&selected=elastic


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
