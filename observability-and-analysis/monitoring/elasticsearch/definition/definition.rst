

.. _elasticsearch_def:

==========================
elasticsearch definition
==========================

.. contents::
   :depth: 3

French wikipedia definition
============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Elasticsearch


Elasticsearch est un serveur utilisant Lucene pour l'indexation et la
recherche des données.

Il fournit un moteur de recherche distribué et multi-entité à travers
une interface REST.
C'est un logiciel libre écrit en Java et publié en open source sous
licence Apache.

Elasticsearch est le serveur de recherche d'entreprise le plus
populaire, suivi par Apache Solr qui utilise aussi Lucene.

Il est associé à deux autres produits libres, Kibana et Logstash,
qui sont respectivement un visualiseur de données et un ETL (initialement
destiné aux logs).

L'indexation des données s'effectue à partir d'une requête HTTP PUT.

La recherche des données s'effectue avec la requête HTTP GET.

Les données échangées sont au format JSON.
