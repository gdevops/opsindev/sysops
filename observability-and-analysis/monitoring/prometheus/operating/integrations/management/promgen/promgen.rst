
.. index::
   pair: configuration ; promgen
   pair: Django ; prometheus
   pair: Django ; promgen

.. _promgen:

===================================================================================================
**promgen** (**Django** based Web UI and configuration generator for Prometheus and Alertmanager)
===================================================================================================

.. seealso::

   - https://github.com/line/promgen
   - https://line.github.io/promgen/
   - http://www.slideshare.net/tokuhirom/promgen-prometheus-managemnet-tool-simpleclientjava-hacks-prometheus-casual
   - https://engineering.linecorp.com/en/blog/how-promgen-routes-notifications/


.. contents::
   :depth: 3


.. include:: README.rst

dockerfile
===========

.. literalinclude:: Dockerfile
   :linenos:

Versions
==========

.. toctree::
   :maxdepth: 3

   versions/versions
