
.. index::
   pair: Postgresql; prometheus

.. _prometheus_postgresql:

=======================================================================
prometheus (The Prometheus monitoring system and time series database)
=======================================================================

.. seealso::

   - https://github.com/timescale/prometheus-postgresql-adapter
