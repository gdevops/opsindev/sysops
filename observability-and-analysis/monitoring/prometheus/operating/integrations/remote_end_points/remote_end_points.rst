

.. _prometheus_operating_integratins_remote:

=======================================================================
prometheus operating integrations remote_end_points
=======================================================================

.. seealso::

   - https://prometheus.io/docs/operating/integrations/#remote-endpoints-and-storage

The remote write and remote read features of Prometheus allow
transparently sending and receiving samples.

This is primarily intended for long term storage.

It is recommended that you perform careful evaluation of any solution
in this space to confirm it can handle your data volumes.


.. toctree::
   :maxdepth: 3

   postgresql/postgresql
