
.. index::
   pair: Monitoring; prometheus
   ! prometheus

.. _prometheus:

=======================================================================
prometheus (The Prometheus monitoring system and time series database)
=======================================================================


- https://en.wikipedia.org/wiki/Prometheus_%28software%29
- https://github.com/prometheus/prometheus
- https://prometheus.io/
- https://x.com/PrometheusIO
- https://landscape.cncf.io/category=observability-and-analysis&format=card-mode&grouping=category&selected=prometheus

.. figure:: Prometheus_software_logo.svg.png
   :align: center


.. figure:: cncf_prometheus.png
   :align: center

   https://landscape.cncf.io/category=observability-and-analysis&format=card-mode&grouping=category&selected=prometheus


.. toctree::
   :maxdepth: 6

   definition/definition
   versions/versions
   clients/clients
   operating/operating
