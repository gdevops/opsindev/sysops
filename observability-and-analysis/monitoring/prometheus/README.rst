Prometheus
==========

|Build Status| |CircleCI| |Docker Repository on Quay| |Docker Pulls| |Go
Report Card| |CII Best Practices|

Visit `prometheus.io <https://prometheus.io>`__ for the full
documentation, examples and guides.

Prometheus, a `Cloud Native Computing Foundation <https://cncf.io/>`__
project, is a systems and service monitoring system. It collects metrics
from configured targets at given intervals, evaluates rule expressions,
displays the results, and can trigger alerts if some condition is
observed to be true.

Prometheus' main distinguishing features as compared to other monitoring
systems are:

-  a **multi-dimensional** data model (timeseries defined by metric name
   and set of key/value dimensions)
-  a **flexible query language** to leverage this dimensionality
-  no dependency on distributed storage; **single server nodes are
   autonomous**
-  timeseries collection happens via a **pull model** over HTTP
-  **pushing timeseries** is supported via an intermediary gateway
-  targets are discovered via **service discovery** or **static
   configuration**
-  multiple modes of **graphing and dashboarding support**
-  support for hierarchical and horizontal **federation**

Architecture overview
---------------------

.. figure:: https://cdn.jsdelivr.net/gh/prometheus/prometheus@c34257d069c630685da35bcef084632ffd5d6209/documentation/images/architecture.svg
   :alt:

Install
-------

There are various ways of installing Prometheus.

Precompiled binaries
~~~~~~~~~~~~~~~~~~~~

Precompiled binaries for released versions are available in the
`*download* section <https://prometheus.io/download/>`__ on
`prometheus.io <https://prometheus.io>`__. Using the latest production
release binary is the recommended way of installing Prometheus. See the
`Installing <https://prometheus.io/docs/introduction/install/>`__
chapter in the documentation for all the details.

Debian packages `are
available <https://packages.debian.org/sid/net/prometheus>`__.

Docker images
~~~~~~~~~~~~~

Docker images are available on
`Quay.io <https://quay.io/repository/prometheus/prometheus>`__ or
`Docker Hub <https://hub.docker.com/r/prom/prometheus/>`__.

You can launch a Prometheus container for trying it out with

::

    $ docker run --name prometheus -d -p 127.0.0.1:9090:9090 prom/prometheus

Prometheus will now be reachable at http://localhost:9090/.

Building from source
~~~~~~~~~~~~~~~~~~~~

To build Prometheus from the source code yourself you need to have a
working Go environment with `version 1.11 or greater
installed <https://golang.org/doc/install>`__.

You can directly use the ``go`` tool to download and install the
``prometheus`` and ``promtool`` binaries into your ``GOPATH``:

::

    $ go get github.com/prometheus/prometheus/cmd/...
    $ prometheus --config.file=your_config.yml

You can also clone the repository yourself and build using ``make``:

::

    $ mkdir -p $GOPATH/src/github.com/prometheus
    $ cd $GOPATH/src/github.com/prometheus
    $ git clone https://github.com/prometheus/prometheus.git
    $ cd prometheus
    $ make build
    $ ./prometheus --config.file=your_config.yml

The Makefile provides several targets:

-  *build*: build the ``prometheus`` and ``promtool`` binaries
-  *test*: run the tests
-  *test-short*: run the short tests
-  *format*: format the source code
-  *vet*: check the source code for common errors
-  *assets*: rebuild the static assets
-  *docker*: build a docker container for the current ``HEAD``

More information
----------------

-  The source code is periodically indexed: `Prometheus
   Core <https://godoc.org/github.com/prometheus/prometheus>`__.
-  You will find a Travis CI configuration in ``.travis.yml``.
-  See the `Community page <https://prometheus.io/community>`__ for how
   to reach the Prometheus developers and users on various communication
   channels.

Contributing
------------

Refer to
`CONTRIBUTING.md <https://github.com/prometheus/prometheus/blob/master/CONTRIBUTING.md>`__

License
-------

Apache License 2.0, see
`LICENSE <https://github.com/prometheus/prometheus/blob/master/LICENSE>`__.

.. |Build Status| image:: https://travis-ci.org/prometheus/prometheus.svg
   :target: https://travis-ci.org/prometheus/prometheus
.. |CircleCI| image:: https://circleci.com/gh/prometheus/prometheus/tree/master.svg?style=shield
   :target: https://circleci.com/gh/prometheus/prometheus
.. |Docker Repository on Quay| image:: https://quay.io/repository/prometheus/prometheus/status
   :target: https://quay.io/repository/prometheus/prometheus
.. |Docker Pulls| image:: https://img.shields.io/docker/pulls/prom/prometheus.svg?maxAge=604800
   :target: https://hub.docker.com/r/prom/prometheus/
.. |Go Report Card| image:: https://goreportcard.com/badge/github.com/prometheus/prometheus
   :target: https://goreportcard.com/report/github.com/prometheus/prometheus
.. |CII Best Practices| image:: https://bestpractices.coreinfrastructure.org/projects/486/badge
   :target: https://bestpractices.coreinfrastructure.org/projects/486
