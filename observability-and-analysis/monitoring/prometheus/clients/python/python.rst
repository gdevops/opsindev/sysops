
.. index::
   pair: prometheus client ; python

.. _prometheus_client_python:

========================================================================================
prometheus client python (Prometheus instrumentation library for Python applications)
========================================================================================


.. seealso::

   - https://github.com/prometheus/client_python



.. include:: README.rst
