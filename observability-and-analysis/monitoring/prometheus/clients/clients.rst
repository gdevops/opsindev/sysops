
.. index::
   pair: clients; prometheus

.. _prometheus_clients:

=============================
prometheus client libraries
=============================

.. seealso::

   - https://prometheus.io/docs/instrumenting/clientlibs/


Before you can monitor your services, you need to add instrumentation
to their code via one of the Prometheus client libraries.

**These implement the Prometheus metric types**.

Choose a Prometheus client library that matches the language in which
your application is written.

This lets you define and expose internal metrics via an HTTP endpoint
on your application’s instance.


.. toctree::
   :maxdepth: 3

   python/python
