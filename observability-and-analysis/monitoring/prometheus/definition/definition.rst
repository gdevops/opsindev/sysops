
.. index::
   pair: Definition; prometheus

.. _prometheus_def:

==========================
prometheus definition
==========================


.. contents::
   :depth: 3

English wikipedia definition
==============================


.. seealso::

   - https://en.wikipedia.org/wiki/Prometheus_%28software%29


Prometheus is an open-source software project written in Go that is
used to record real-time metrics in a time series database (allowing for
high dimensionality) built using a HTTP pull model, with flexible queries
and real-time alerting.

The project is licensed under the Apache 2 License, with source code
available on GitHub, and is a graduated project of the Cloud Native
Computing Foundation, along with Kubernetes and Envoy.


Architecture
==============

A typical monitoring platform with Prometheus is composed of multiple
tools:

- Multiple exporters that typically run on the monitored host to export
  local metrics.
- Prometheus to centralize and store the metrics.
- Alertmanager to trigger alerts based on those metrics.
- :ref:`Grafana to produce dashboards <grafana_dashboards>`.
- PromQL is the query language used to create dashboard and alerts.

Data storage format
=====================

Prometheus data is stored in the form of metrics, with each metric having
a name that is used for referencing and querying it.

Each metric can be drilled down by an arbitrary number of labels.

Labels can include information on the data source (which server the data
is coming from) and other application-specific breakdown information
such as the HTTP status code (for metrics related to HTTP responses),
query method (GET versus POST), endpoint, etc.

The ability to specify an arbitrary list of labels and to query based
on these in real time is why Prometheus' data model is called multi-dimensional.

Prometheus stores data locally on disk, which helps for fast data storage
and fast querying.

Data collection
=================

**Prometheus collects data in the form of time series**.

The time series are built through a pull model: the Prometheus server
queries a list of data sources (sometimes called exporters) at a specific
polling frequency. Each of the data sources serves the current values
of the metrics for that data source at the endpoint queried by Prometheus.

The Prometheus server then aggregates data across the data sources.

Prometheus has a number of mechanisms to automatically discover
resources that it should be using as data sources.

Alerts and monitoring
=======================

Configuration for alerts can be specified in Prometheus that specifies
a condition that needs to be maintained for a specific duration in order
for an alert to trigger.

When alerts trigger, they are forwarded to Alertmanager, another
Prometheus service.

Alertmanager can include logic to silence alerts and also to forward
them to email, Slack, or notification services such as PagerDuty.

Dashboards
============

.. seealso::

   - :ref:`grafana_dashboards`

Prometheus is not intended as a dashboarding solution.

Although it can be used to graph specific queries, it is not a full-fledged
dashboarding solution and :ref:`needs to be hooked up with Grafana <grafana_dashboards>`
to generate dashboards;
this has been cited as a disadvantage due to the additional setup complexity.

Interoperability
==================

.. seealso::

   - :ref:`prometheus_postgresql`

Prometheus favors white-box monitoring.

Applications are encouraged to publish (export) internal metrics to be
collected periodically by Prometheus.

Some exporters and agents for various applications are available to
provide metrics.

Prometheus supports some monitoring and administration protocols to
allow interoperability for transitioning: Graphite, StatsD, SNMP, JMX,
and CollectD.

Prometheus focuses on the availability of the platform and basic
operations.

The metrics are typically stored for few weeks.
For long term storage, the metrics can be migrated to a third-party
databases like Cortex, CrateDB, InfluxDB, :ref:`PostgreSQL/TimescaleDB <prometheus_postgresql>`, etc.


.. include:: ../README.rst
