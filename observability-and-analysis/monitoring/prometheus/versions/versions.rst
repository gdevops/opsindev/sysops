
.. _prometheus_versions:

=======================================================================
prometheus versions
=======================================================================

.. seealso::

   - https://github.com/prometheus/prometheus
   - https://github.com/prometheus/prometheus/releases
   - https://github.com/prometheus/prometheus/blob/master/CHANGELOG.md

.. toctree::
   :maxdepth: 3

   2.17.2/2.17.2
   2.9.2/2.9.2
   2.9.1/2.9.1
   2.8.1/2.8.1
