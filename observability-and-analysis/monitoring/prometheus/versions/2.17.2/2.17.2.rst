
.. index::
   pair: prometheus ; 2.17.2 (2020-04-20)

.. _prometheus_2_17_2:

=======================================================================
prometheus 2.17.2 (2020-04-20)
=======================================================================

.. seealso::

   - https://github.com/prometheus/prometheus/tree/v2.17.2
   - https://github.com/prometheus/prometheus/releases/tag/v2.17.2
   - https://github.com/prometheus/prometheus/blob/master/CHANGELOG.md
