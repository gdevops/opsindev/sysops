
.. index::
   pair: Monitoring; EFK
   ! EFK

.. _efk:

=======================================
EFK (Elasticsearch, Fluentd, kibana)
=======================================

.. seealso::

   - https://www.elastic.co/fr/
   - https://en.wikipedia.org/wiki/Extract,_transform,_load


Elasticsearch
===============

- :ref:`elasticsearch`


Fluentd
=========

- :ref:`fluentd`


Kibana
========

- :ref:`kibana`
