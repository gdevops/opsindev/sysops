
.. index::
   pair: Monitoring; datadog
   ! datadog

.. _datadog:

==============================================================
**datadog** (private, partial open source)
==============================================================

.. seealso::

   - https://en.wikipedia.org/wiki/Datadog
   - https://github.com/DataDog
   - https://github.com/DataDog/datadog-agent
   - https://www.datadoghq.com/
   - https://x.com/datadoghq


.. figure:: logo_datadog.png
   :align: center
   :width: 300


.. toctree::
   :maxdepth: 3
