
.. index::
   pair: grafana dashboard ; django
   pair: python ; django


.. _grafana_django_dashboard:

=========================================================================================================================
grafana django dashboard
=========================================================================================================================

.. seealso::

   - https://grafana.com/dashboards/9528
   - https://github.com/korfuri/django-prometheus/


.. figure:: django_dashboard.png
   :align: center

.. include:: README.rst
