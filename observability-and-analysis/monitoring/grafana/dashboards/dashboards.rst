
.. index::
   pair: dashboards; grafana

.. _grafana_dashboards:

=========================================================================================================================
grafana dashboards
=========================================================================================================================

.. seealso::

   - https://grafana.com/dashboards

.. toctree::
   :maxdepth: 3

   django/django
