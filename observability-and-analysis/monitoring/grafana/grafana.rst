.. index::
   pair: Data visualization; grafana
   ! grafana

.. _grafana:

==============================================================================================================================
|grafana| **grafana** (The open and composable observability and data visualization platform)
==============================================================================================================================

- https://fr.wikipedia.org/wiki/Grafana
- https://github.com/grafana/grafana
- https://github.com/grafana/grafana/commits.atom
- https://grafana.com/
- https://x.com/grafana
- https://grafana.com/blog/index.xml
- https://bestpractices.coreinfrastructure.org/en/projects/3252


.. grid:: 1 2 3 4
    :gutter: 1
    :class-container: container pb-2


    .. grid-item-card::
        :img-top: images/cncf_grafana_2023.png
        :class-img-top: pt-2 w-40 d-block mx-auto fixed-height-img

        .. button-link:: https://landscape.cncf.io/?selected=grafana

           CNCF Cloud Native Interactive Landscape Grafana



.. toctree::
   :maxdepth: 3


   definition/definition
   dashboards/dashboards
   versions/versions
