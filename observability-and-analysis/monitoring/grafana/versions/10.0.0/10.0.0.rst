
.. index::
   pair: 10.0.0 (2023-06-12); grafana

.. _grafana_10_0_0:

=========================================
**grafana 10.0.0** (2023-06-12)
=========================================

- https://grafana.com/docs/grafana/latest/whatsnew/whats-new-in-v10-0/
