
.. index::
   pair: def; grafana

.. _grafana_def:

==============================================================
|grafana| grafana definition
==============================================================

- https://grafana.com/docs/

Github definitions
======================

- https://github.com/grafana/grafana/blob/main/README.md

**The open and composable observability and data visualization platform.**

Visualize metrics, logs, and traces from multiple sources like Prometheus,
Loki, Elasticsearch, InfluxDB, **Postgres** and many more.

Grafana allows you to query, visualize, alert on and understand your
metrics no matter where they are stored.

Create, explore, and share dashboards with your team and **foster a data-driven
culture**:

- Visualizations: Fast and flexible client side graphs with a multitude
  of options.
  Panel plugins offer many different ways to visualize metrics and logs.
- Dynamic Dashboards: Create dynamic & reusable dashboards with template
  variables that appear as dropdowns at the top of the dashboard.
- Explore Metrics: Explore your data through ad-hoc queries and dynamic
  drilldown. Split view and compare different time ranges, queries and
  data sources side by side.
- Explore Logs: Experience the magic of switching from metrics to logs
  with preserved label filters.
  Quickly search through all your logs or streaming them live.
- Alerting: Visually define alert rules for your most important metrics.
  Grafana will continuously evaluate and send notifications to systems
  like Slack, PagerDuty, VictorOps, OpsGenie.
- Mixed Data Sources: Mix different data sources in the same graph!
  You can specify a data source on a per-query basis.
  This works for even custom datasources.



French wikipedia definition
============================

- https://fr.wikipedia.org/wiki/Grafana


Grafana est un logiciel libre sous licence Apache 2.02 qui permet la
visualisation et la mise en forme de données métriques.

Il permet de réaliser des tableaux de bord et des graphiques depuis
plusieurs sources dont des bases de données de série temporelle
(Time Series Database) comme Graphite, InfluxDB et OpenTSDB


Grafana est multiplateformes sans aucune dépendance et peut également
être déployé avec Docker. Il est écrit en Go et dispose d’une API
HTTP complète.

Outre la gestion des tableaux de bord classique (ajouts, suppression,
favoris), Grafana propose le partage d'un tableau de bord actuel en
créant un lien ou en créant un instantané statique de celui-ci.

Tous les tableaux de bord et toutes les sources de données sont liés à
une organisation et les utilisateurs de l'application sont liés aux
organisations via des rôles.

Grafana empêche les utilisateurs d’écraser accidentellement un tableau
de bord. Une protection similaire existe lors de la création d'un
nouveau tableau de bord dont le nom existe déjà.

L'outil offre la possibilité de mettre en place des alertes.

Wikipédia utilise Grafana (via Wikimédia).

Aussi, Wikimédia donne libre accès à ses statistiques : https://grafana.wikimedia.org/.
