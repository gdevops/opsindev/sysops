
.. index::
   pair: Monitoring; sentry
   ! sentry

.. _sentry:

====================================================================
**sentry** application monitoring, with a focus on error reporting
====================================================================

.. seealso::

   - https://github.com/getsentry/sentry
   - https://sentry.io/welcome/


.. figure:: logo_sentry.png
   :align: center
   :width: 300
