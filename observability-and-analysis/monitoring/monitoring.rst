
.. index::
   pair: Logs ; Management
   ! Monitoring

.. _monitoring:

==========================
Monitoring
==========================

- https://en.wikipedia.org/wiki/Comparison_of_network_monitoring_systems
- https://en.wikipedia.org/wiki/Computer_and_network_surveillance
- https://fr.wikipedia.org/wiki/Gestion_des_logs
- https://en.wikipedia.org/wiki/Log_management
- https://openapm.io/landscape
- https://landscape.cncf.io/guide#observability-and-analysis--monitoring

.. figure:: images/softwares.webp

.. toctree::
   :maxdepth: 6

   definition/definition
   alignak/alignak
   cadvisor/cadvisor
   datadog/datadog
   grafana/grafana
   elasticsearch/elasticsearch
   kibana/kibana
   logstash/logstash
   fluentd/fluentd
   efk/efk
   elk/elk
   glances/glances
   prometheus/prometheus
   sentry/sentry
   shinken/shinken
