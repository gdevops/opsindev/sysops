
.. index::
   pair: Monitoring tool ; alignak
   ! alignak

.. _alignak:

=========================================================================
**alignak** (Monitoring tool, highly flexible and new standard oriented)
=========================================================================

.. seealso::

   - https://github.com/Alignak-monitoring
   - http://docs.alignak.net/en/latest/
   - http://www.alignak.net/
   - https://github.com/Alignak-monitoring/alignak
   - http://www.alignak.net/documentation/
   - https://github.com/ddurieux
   - https://github.com/mohierf

.. figure:: logo_alignak.png
   :align: center
   :width: 300


.. seealso:: https://github.com/Alignak-monitoring/alignak/commit/9f8c30dc123b354375a42a8b6eaf9fcc6aa37b0a

::


                  u"   █████╗ ██╗     ██╗ ██████╗ ███╗   ██╗ █████╗ ██╗  ██╗",
                  u"  ██╔══██╗██║     ██║██╔════╝ ████╗  ██║██╔══██╗██║ ██╔╝",
                  u"  ███████║██║     ██║██║  ███╗██╔██╗ ██║███████║█████╔╝ ",
                  u"  ██╔══██║██║     ██║██║   ██║██║╚██╗██║██╔══██║██╔═██╗ ",
                  u"  ██║  ██║███████╗██║╚██████╔╝██║ ╚████║██║  ██║██║  ██╗",
                  u"  ╚═╝  ╚═╝╚══════╝╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝  ╚═╝╚═╝  ╚═╝",




.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
   web_ui/web_ui
