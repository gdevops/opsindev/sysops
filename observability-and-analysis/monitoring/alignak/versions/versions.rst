
.. index::
   pair: alignak; versions

.. _alignak_versions:

==============================================================
**alignak** versions
==============================================================

.. seealso::

   - https://github.com/Alignak-monitoring/alignak
   - https://github.com/Alignak-monitoring/alignak/releases

.. toctree::
   :maxdepth: 3

   2.1.5/2.1.5
   2.1.3/2.1.3
