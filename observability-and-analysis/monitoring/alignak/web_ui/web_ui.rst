
.. index::
   pair: alignak; webui

.. _alignak_webui:

==============================================================
**alignak-webui**
==============================================================

.. seealso::

   - https://github.com/Alignak-monitoring-contrib/alignak-webui
   - http://docs.alignak.net/en/latest/
   - http://docs.alignak.net/en/latest/05_extending_alignak/05_alignak_webui.html
   - https://alignak-web-ui.readthedocs.io/
   - https://github.com/Alignak-monitoring-contrib/alignak-webui/blob/develop/docs/source/index.rst


.. toctree::
   :maxdepth: 3

   versions/versions
