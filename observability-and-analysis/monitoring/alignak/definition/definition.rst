

.. _alignak_def:

==============================================================
**alignak**  definition
==============================================================

.. seealso::

   - https://github.com/Alignak-monitoring/alignak
   - http://docs.alignak.net/en/latest/


.. include:: README.rst
