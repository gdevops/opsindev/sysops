
.. index::
   pair: Monitoring; fluentd
   ! fluentd

.. _fluentd:

========================================================================
**Fluentd** (open source data collector for unified logging layer)
========================================================================

.. seealso::

   - https://en.wikipedia.org/wiki/Fluentd
   - https://www.fluentd.org
   - https://github.com/fluent/fluentd/
   - https://landscape.cncf.io/category=logging&format=card-mode&grouping=category&selected=fluentd
   - https://www.fluentd.org/blog/feed.rss
   - https://bestpractices.coreinfrastructure.org/en/projects/1189
   - https://x.com/fluentd
   - https://github.com/frsyuki
   - https://x.com/frsyuki
   - https://x.com/repeatedly
   - https://landscape.cncf.io/category=logging&format=card-mode&grouping=category

.. figure:: cncf_fluentd.png
   :align: center

   https://landscape.cncf.io/category=logging&format=card-mode&grouping=category&selected=fluentd


.. figure:: landscape_cncf.png
   :align: center
   :width: 300

   https://landscape.cncf.io/category=logging&format=card-mode&grouping=category

.. toctree::
   :maxdepth: 3

   definition/definition
   articles/articles
   versions/versions
   cncf/cncf
