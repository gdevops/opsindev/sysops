
.. _fluentd_def:

==============================================================
fluentd definition
==============================================================

.. seealso::

   - https://www.fluentd.org
   - https://github.com/fluent/fluentd/
   - https://www.fluentd.org/blog/unified-logging-layer

.. contents::
   :depth: 3


github fluentd definition
==========================

.. seealso::

   - https://github.com/fluent/fluentd/blob/v1.4.2/README.md


.. include:: README.rst
