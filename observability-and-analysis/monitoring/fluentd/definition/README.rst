Fluentd: Open-Source Log Collector
==================================

`<https://travis-ci.org/fluent/fluentd>`__ |Code Climate| |CII Best
Practices| |FOSSA Status|

`Fluentd <https://www.fluentd.org/>`__ collects events from various data
sources and writes them to files, RDBMS, NoSQL, IaaS, SaaS, Hadoop and
so on. Fluentd helps you unify your logging infrastructure (Learn more
about the `Unified Logging
Layer <https://www.fluentd.org/blog/unified-logging-layer>`__).


An event consists of *tag*, *time* and *record*. Tag is a string
separated with '.' (e.g. myapp.access). It is used to categorize events.
Time is a UNIX time recorded at occurrence of an event. Record is a JSON
object.

Example Use Cases
-----------------

+-----------+---------------+--------------+
| Use Case  | Description   | Diagram      |
+===========+===============+==============+
| Centraliz | Fluentd can   |              |
| ing       | be used to    |              |
| Apache/Ng | tail          |              |
| inx       | access/error  |              |
| Server    | logs and      |              |
| Logs      | transport     |              |
|           | them reliably |              |
|           | to remote     |              |
|           | systems.      |              |
+-----------+---------------+--------------+
| Syslog    | Fluentd can   |              |
| Alerting  | "grep" for    |              |
|           | events and    |              |
|           | send out      |              |
|           | alerts.       |              |
+-----------+---------------+--------------+
| Mobile/We | Fluentd can   |              |
| b         | function as   |              |
| Applicati | middleware to |              |
| on        | enable        |              |
| Logging   | asynchronous, |              |
|           | scalable      |              |
|           | logging for   |              |
|           | user action   |              |
|           | events.       |              |
+-----------+---------------+--------------+

Quick Start
-----------

::

    $ gem install fluentd
    $ fluentd -s conf
    $ fluentd -c conf/fluent.conf &
    $ echo '{"json":"message"}' | fluent-cat debug.test

Development
-----------

Prerequisites
~~~~~~~~~~~~~

-  Ruby 2.1 or later
-  git

``git`` should be in ``PATH``. On Windows, you can use
``Github for Windows`` and ``GitShell`` for easy setup.

Install dependent gems
~~~~~~~~~~~~~~~~~~~~~~

Use bundler:

::

    $ gem install bundler
    $ bundle install --path vendor/bundle

Run test
~~~~~~~~

::

    $ bundle exec rake test

You can run specified test via ``TEST`` environment variable:

::

    $ bundle exec rake test TEST=test/test_specified_path.rb
    $ bundle exec rake test TEST=test/test_*.rb

Fluentd UI: Admin GUI
---------------------

`Fluentd UI <https://github.com/fluent/fluentd-ui>`__ is a graphical
user interface to start/stop/configure Fluentd.



More Information
----------------

-  Website: https://www.fluentd.org/
-  Documentation: https://docs.fluentd.org/
-  Project repository: https://github.com/fluent
-  Discussion: https://groups.google.com/group/fluentd
-  Slack / Community: https://slack.fluentd.org
-  Newsletters: https://www.fluentd.org/newsletter
-  Author: `Sadayuki Furuhashi <https://github.com/frsyuki>`__
-  Copyright: 2011-2018 Fluentd Authors
-  License: Apache License, Version 2.0

Contributors:
-------------

Patches contributed by `great
developers <https://github.com/fluent/fluentd/contributors>`__.

` <https://github.com/fluent/fluentd>`__

.. |Code Climate| image:: https://codeclimate.com/github/fluent/fluentd/badges/gpa.svg
   :target: https://codeclimate.com/github/fluent/fluentd
.. |CII Best Practices| image:: https://bestpractices.coreinfrastructure.org/projects/1189/badge
   :target: https://bestpractices.coreinfrastructure.org/projects/1189
.. |FOSSA Status| image:: https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgithub.com%2Ffluent%2Ffluentd.svg?type=shield
   :target: https://app.fossa.io/projects/git%2Bhttps%3A%2F%2Fgithub.com%2Ffluent%2Ffluentd?ref=badge_shield
