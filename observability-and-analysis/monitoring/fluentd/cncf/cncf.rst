
.. index::
   pair: CNCF; fluentd

.. _fluentd_cncf:

==============================================================
fluentd CNCF (2019-04-11)
==============================================================

.. seealso::

   - https://landscape.cncf.io/


.. contents::
   :depth: 3


https://www.cncf.io/announcement/2019/04/11/cncf-announces-fluentd-graduation/
================================================================================

SAN FRANCISCO, Calif. – April 11, 2019 – The Cloud Native Computing Foundation®
(CNCF®), which sustains open source technologies like Kubernetes® and Prometheus™,
today announced that Fluentd is its sixth project to graduate, following
Kubernetes, Prometheus, Envoy, CoreDNS and containerd.

To move from the maturity level of incubation to graduation, projects must
demonstrate thriving adoption, a documented, structured governance process,
and a strong commitment to community sustainability and inclusivity.


https://www.fluentd.org/blog/fluentd-cncf-graduation
=======================================================

.. seealso::

   - https://www.fluentd.org/blog/fluentd-cncf-graduation


We are thrilled to announce that Fluentd has become the sixth CNCF Graduated
Project !

This is a huge recognition to the project and its community.

Fluentd wide adoption by the Enterprise and contributions are in a continuous
grow: we have more than 900 contributed plugins and more than 100k downloads
a day !

Fluentd was created in 2011 by Sadayuki “Sada” Furuhashi, co-founder of
Treasure Data, Inc., as an open source data collector for building a
Unified Logging Layer, which unifies the data collection and consumption
for better use and understanding of data.

In November 2016, Fluentd was accepted as CNCF’s sixth hosted project after
Kubernetes, Prometheus, and OpenTracing.

As of today (2019-04-11), Fluentd is used by major companies to solve log
collection and processing at scale: Microsoft, Redhat, and Google are a
few names of its known users.

Fluentd's core maintainer, Masahiro Nakagawa (well known as @repeatedly on Twitter),
said that he's thrilled to have had the opportunity to collaborate with the
CNCF community and to grow its user base, and it's an honor to see Fluentd
graduated.

Today we celebrate this big accomplishment, and we continue looking forward
to improving on upcoming Logging and data processing challenges.
