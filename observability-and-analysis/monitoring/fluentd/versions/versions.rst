
.. index::
   pair: Versions; fluentd

.. _fluentd_versions:

==============================================================
fluentd versions
==============================================================

.. seealso::

   - https://github.com/fluent/fluentd/releases
   - https://github.com/fluent/fluentd/blob/master/CHANGELOG.md

.. toctree::
   :maxdepth: 3

   1.10.2/1.10.2
   1.4.2/1.4.2
