
.. index::
   pair: Versions; Thruk

.. _thruk_versions:

=============================================
**Thruk** versions
=============================================

.. seealso::

   - https://github.com/sni/Thruk


.. toctree::
   :maxdepth: 3

   2.28/2.28
