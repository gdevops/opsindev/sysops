

.. _thruk_def:

=================================
**Thruk** definition
=================================

.. seealso::

   - https://github.com/sni/Thruk



Thruk is a multibackend monitoring webinterface which currently supports
Nagios, Naemion, Icinga and Shinken as backend using the Livestatus API.

It is designed to be a 'dropin' replacement and covers the original
features plus adds additional enhancements for large installations,
increased usability and many usefull addons.
