
.. index::
   pair: GUI; Thruk

.. _thruk:

==========================================================================================================================
**Thruk** Thruk is a multibackend monitoring webinterface for Naemon, Nagios, Icinga and Shinken using the Livestatus API
==========================================================================================================================

.. seealso::

   - https://github.com/sni/Thruk


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
