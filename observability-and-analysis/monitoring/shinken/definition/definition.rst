
.. index::
   pair: Definition; shinken

.. _shinken_def:

==============================================================
**shinken** definition
==============================================================

.. contents::
   :depth: 3


Shinken definition
===================

Shinken is an open source monitoring framework written in Python under
the terms of the GNU Affero General Public License.

It was created in 2009 as a simple proof of concept of a Nagios patch.

The first release of Shinken was the December 1st of 2009 as simple
monitoring tool. Since the 2.0 version (April 2014) Shinken is described
as a monitoring framework due to its high number of modules.

For the same reason, modules are now in separated repositories


English wikipedia definition
===============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Shinken_%28logiciel%29
   - https://en.wikipedia.org/wiki/Shinken_(software)


Shinken is an open source computer system and network monitoring software
application compatible with Nagios.

It watches hosts and services, gathers performance data and alerts users
when error conditions occur and again when the conditions clear.

Shinken's architecture aims to offer easier load balancing and high
availability. The administrator manages a single configuration, the
system automatically "cuts" it into parts and dispatches it to worker
nodes.

It takes its name from this functionality: a Shinken is a Japanese sword.

Shinken was written by Jean Gabès as a proof of concept for a new Nagios
architecture. Believing the new implementation was faster and more
flexible than the old C code, he proposed it as the new development
branch of Nagios 4.

This proposal was turned down by the Nagios authors, so Shinken became
an independent network monitoring software application compatible with Nagios.

Shinken is designed to run under all operating systems where Python runs.

The development environment is under Linux, but also runs well on other
Unix variants and Windows.

The reactionner process (responsible for sending notifications) can also
be run under the Android OS. It is free software, licensed under the
terms of the Affero General Public License as published by the
Free Software Foundation.


French wikipedia definition
===============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Shinken_%28logiciel%29


Shinken est une application permettant la surveillance système et réseau.

Elle surveille les hôtes et services spécifiés, alertant lorsque les
systèmes vont mal et quand ils vont mieux.

C'est un logiciel libre sous licence GNU AGPL. Elle est complètement
compatible avec le logiciel Nagios et elle a pour but d'apporter une
supervision distribuée et hautement disponible facile à mettre en place.

Démarrée comme une preuve de concept pour Nagios sur les architectures
distribuées, le programme a rapidement démontré des performances et une
flexibilité bien plus importantes que son aîné Nagios[réf. nécessaire].

À la suite d'un refus en décembre 2009 des développeurs de Nagios de
voir Shinken devenir la nouvelle branche de développement de Nagios
dans le futur, Shinken peut désormais être considéré comme un projet
indépendant de système de surveillance système et réseau.

En mai 2015, le développeur principal annonce lors de la sortie de la
version 2.4 du logiciel qu'une fourche (Alignak) est créée.
