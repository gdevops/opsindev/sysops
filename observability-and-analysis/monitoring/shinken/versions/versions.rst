
.. index::
   pair: Versions; shinken

.. _shinken_versions:

==============================================================
**shinken** versions
==============================================================

.. seealso::

   - https://github.com/naparuba/shinken

.. toctree::
   :maxdepth: 3

   2.4.3/2.4.3
   2.4.0/2.4.0
