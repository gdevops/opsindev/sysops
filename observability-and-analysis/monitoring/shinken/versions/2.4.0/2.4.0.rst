
.. index::
   pair: 2.4.0; shinken

.. _shinken_2_4_0:

==============================================================
**shinken** 2.4.0 (2015-05-04)
==============================================================

.. seealso::

   - https://github.com/naparuba/shinken/tree/2.4
   - https://linuxfr.org/news/shinken-2-4


.. contents::
   :depth: 3

Le projet devient grand, il est forké :)
===========================================

Deux membres de la communauté ont eu un désaccord majeur sur la
gouvernance du projet.

Ils demandaient la mise en place d'un comité de direction du projet à
la place du leader du projet, afin de pouvoir valider ses commits
notamment. Mais ceci a été décliné par ce dernier.

D'après lui, si cette organisation est efficace pour des gros projets
du genre OpenStack, ça n'a pas de sens pour un projet de la taille
de Shinken.

Ces deux membres ont donc décidé de forker le projet afin d'appliquer
leur nouvelle gouvernance. Leur nouveau fork se nomme Alignak. Nous
leur souhaitons toute la réussite dans leur nouveau projet dans le
monde décidément impitoyable de la supervision :)

La prochaine version
======================

La prochaine version du framework Shinken sera concentrée une nouvelle
fois sur les performances sur les très gros environnements (plus de 50K
points de collecte) afin que ces derniers puissent encore plus descendre
les intervalles de collecte d'information et donc avoir une supervision
plus précise.
