
.. index::
   pair: Monitoring; shinken
   ! shinken

.. _shinken:

==============================================================
**shinken** Flexible and scalable monitoring framework
==============================================================

.. seealso::

   - https://github.com/naparuba/shinken
   - https://en.wikipedia.org/wiki/Shinken_(software)
   - http://shinken.io/
   - http://www.shinken-monitoring.org/


.. figure:: mascote_shinken.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
   thruk/thruk
