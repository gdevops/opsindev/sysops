
.. index::
   pair: Monitoring; cadvisor
   pair: Monitoring; Container Advisor)

.. _cadvisor:

================================================================================================================
**cadvisor** (Container Advisor) analyzes resource usage and performance characteristics of running containers
================================================================================================================

.. seealso::

   - https://github.com/google/cadvisor

.. figure:: logo_cadvisor.png
   :align: center

   Logo cadvisor


.. contents::
   :depth: 3


Description
============

**cAdvisor** (Container Advisor) provides container users an understanding
of the resource usage and performance characteristics of their running
containers.

It is a running daemon that collects, aggregates, processes, and exports
information about running containers.

Specifically, for each container it keeps resource isolation parameters,
historical resource usage, histograms of complete historical resource
usage and network statistics.

This data is exported by container and machine-wide.

cAdvisor has native support for Docker containers and should support
just about any other container type out of the box.

We strive for support across the board so feel free to open an issue if
that is not the case. cAdvisor's container abstraction is based on
lmctfy's so containers are inherently nested hierarchically.
