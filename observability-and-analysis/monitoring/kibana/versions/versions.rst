
.. index::
   pair: Versions; Kibana
   ! Kibana

.. _kibana_versions:

===============================================
Kibana versions
===============================================

- https://github.com/elastic/kibana/releases

.. toctree::
   :maxdepth: 3

   8.8.2/8.8.2
   7.6.2/7.6.2
