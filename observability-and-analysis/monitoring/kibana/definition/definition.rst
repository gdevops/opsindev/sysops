
.. _kibana_def:

===============================================
Kibana definition
===============================================

.. contents::
   :depth: 3

Définition wikipedia
=======================

.. seealso::

   - https://fr.wikipedia.org/wiki/Kibana


Kibana est un greffon de visualisation de données pour Elasticsearch
publié sous la licence libre Apache version 2.

Il fournit des fonctions de visualisation sur du contenu indexé dans
une grappe Elasticsearch.

Les utilisateurs peuvent créer des diagrammes en barre, en ligne, des
nuages de points, des camemberts et des cartes de grands volumes de données.


Elastic definition
==================

.. seealso::

   - https://www.elastic.co/fr/kibana

Kibana vous permet de visualiser vos données Elasticsearch et de naviguer
dans la Suite Elastic.

Vous voulez effectuer le suivi de la charge de travail liée à la recherche
ou comprendre le flux des requêtes dans vos applications ?

Kibana est là pour ça.
