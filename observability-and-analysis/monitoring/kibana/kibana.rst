
.. index::
   pair: Logs; Kibana
   ! Kibana

.. _kibana:

===============================================
Kibana (Your window into the Elastic Stack)
===============================================

.. seealso::

   - https://github.com/elastic/kibana
   - https://www.elastic.co/fr/kibana
   - https://fr.wikipedia.org/wiki/Kibana
   - https://www.elastic.co/fr/kibana


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
