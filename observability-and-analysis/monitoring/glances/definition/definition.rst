

.. _glances_def:

==============================================================
**glances** definition
==============================================================


Glances an Eye on your system.

A top/htop alternative for GNU/Linux, BSD, Mac OS and Windows operating
systems.


.. include:: README.rst
