
.. index::
   pair: Monitoring; glances
   ! glances

.. _glances:

==============================================================
**glances**
==============================================================

.. seealso::

   - hhttps://github.com/nicolargo/glances
   - https://nicolargo.github.io/glances/
   - https://glances.readthedocs.io/en/latest/
   - https://x.com/nicolargo

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
