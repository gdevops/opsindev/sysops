
.. index::
   pair: Versions; glances

.. _glances_versions:

==============================================================
**glances** versions
==============================================================

.. seealso::

   - https://github.com/nicolargo/glances/releases

.. toctree::
   :maxdepth: 3

   3.1.0/3.1.0
   3.0.0/3.0.0
