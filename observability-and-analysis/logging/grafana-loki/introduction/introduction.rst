
.. _grafana_loki_intro:

=======================================================================================================
Introduction
=======================================================================================================

- https://github.com/grafana/loki

Loki is like Prometheus, but for logs: we prefer a multidimensional label-based
approach to indexing, and want a single-binary, easy to operate system with no
dependencies.

Loki differs from Prometheus by focusing on logs instead of metrics, and delivering
logs via push, instead of pull.

