.. index::
   pair: Logging ; Observability and Analysis
   ! Logging

.. _logging:

=======================================================================================================
Logging
=======================================================================================================

- https://landscape.cncf.io/guide#observability-and-analysis--logging

.. figure:: images/loggers.webp

What it is ?
=====================

Applications emit a steady stream of log messages describing what they are doing
at any given time. These log messages capture various events happening in the
system such as failed or successful actions, audit information, or health events.

Logging tools collect, store, and analyze these messages to track error reports
and related data.

Along with metrics and tracing, logging is one of the pillars of observability.


CNCF projects
================

.. toctree::
   :maxdepth: 3

   grafana-loki/grafana-loki
